<?php
###################################
# Add new version number here.    #
###################################
$version = "2.3.0";

// No further changes required.
echo 'v ' . $version;
echo '<br>';
echo '<span class="date">' . date('Y-m-d', filemtime(__FILE__)) . '</span>';