<!DOCTYPE HTML> 
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="UTF-8">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/translations.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/fm.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/canvas.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/canvas2image.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/base64.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/farbtastic.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/style.css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/canvas.css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/farbtastic.css" />
		<link rel="icon" type="image/x-icon" href="<?=base_url()?>img/favicon.ico" size="16x16" /> 
		
		<title>Fata Morgana</title> 
		
		<!--[if IE]><script type="text/javascript" src="<?=base_url()?>js/excanvas.js"></script><![endif]-->
	</head>
	<body>
        <div id="userInfoBox"></div>
		<div id="modeBar" class="clearfix">
			<h1 class="fatafont">fata morgana</h1>
			<h5><span data-trans="logged-in"></span> <span id="owner-name"></span></h5>
            <div class="mode-menu">
                <div class="mode-switch click active-mode" id="mode-normal" data-trans="mode-map"></div>
                <div class="mode-switch click" id="mode-draw" data-trans="mode-draw"></div>
                <div class="mode-switch click" id="mode-planner" data-trans="mode-plan"></div>
                <div class="help-switch click" id="mode-help" data-trans="mode-help"></div>
            </div>
		</div>
		<div id="townBar" class="clearfix hideme">
            <h2 id="townInfo">
                <span id="townDay"></span>
                <span id="townName"></span>
                <span id="townID"></span>
                <span id="townSpy"><a href="#" target="_new" data-trans="spylink"></a></span>
                <span id="townHistory"><span data-trans="day"></span> </span>
            </h2>
        </div>
		<div id="help-section" class="hidefaq">
			<h2 data-trans="faq-title"></h2>
			<ul>
				<li><h4 data-trans="faq-q1"></h4><p data-trans="faq-a1"></p></li>
				<li><h4 data-trans="faq-q2"></h4><p data-trans="faq-a2"></p></li>
				<li><h4 data-trans="faq-q3"></h4><p data-trans="faq-a3"></p></li>
				<li><h4 data-trans="faq-q4"></h4><p data-trans="faq-a4"></p></li>
			</ul>
		</div>
		<div id="fm-container" class="clearfix tl">
			<div id="fm-content" class="clearfix">
