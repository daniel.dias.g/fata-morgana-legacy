<div id="map-wrapper" class="clearfix">
    <div id="map" class="clearfix"></div>
    <div id="canvasDrawDiv" class="hideme"></div>
    <div id="canvasPlanDiv" class="hideme"></div>
</div>
<div id="ruinmap-wrapper" class="hideme">
    <div id="ruinmap-wrapper-close" data-trans="close"></div>
    <div id="ruinInfoBox">
        <div id="ruinTileBox">
            <h3 data-trans="hallways"></h3>
            <div class="ruinOption ruinTile tile-0" tile="0" data-transtitle="hallway-none"></div>
            <div class="ruinOption ruinTile tile-1" tile="1" data-transtitle="hallway-n"></div>
            <div class="ruinOption ruinTile tile-2" tile="2" data-transtitle="hallway-e"></div>
            <div class="ruinOption ruinTile tile-3" tile="3" data-transtitle="hallway-s"></div>
            <div class="ruinOption ruinTile tile-4" tile="4" data-transtitle="hallway-w"></div>
            <div class="ruinOption ruinTile tile-5" tile="5" data-transtitle="hallway-ns"></div>
            <div class="ruinOption ruinTile tile-6" tile="6" data-transtitle="hallway-ew"></div>
            <div class="ruinOption ruinTile tile-7" tile="7" data-transtitle="hallway-news"></div>
            <div class="ruinOption ruinTile tile-8" tile="8" data-transtitle="hallway-ne"></div>
            <div class="ruinOption ruinTile tile-9" tile="9" data-transtitle="hallway-es"></div>
            <div class="ruinOption ruinTile tile-10" tile="10" data-transtitle="hallway-ws"></div>
            <div class="ruinOption ruinTile tile-11" tile="11" data-transtitle="hallway-nw"></div>
            <div class="ruinOption ruinTile tile-12" tile="12" data-transtitle="hallway-new"></div>
            <div class="ruinOption ruinTile tile-13" tile="13" data-transtitle="hallway-nes"></div>
            <div class="ruinOption ruinTile tile-14" tile="14" data-transtitle="hallway-ews"></div>
            <div class="ruinOption ruinTile tile-15" tile="15" data-transtitle="hallway-nws"></div>
        </div>
        <div id="ruinDoorLockBox">
            <h3 data-trans="doorlocks"></h3>
            <div class="ruinOption ruinDoorLock doorlock-0" doorlock="0" data-transtitle="doorlock-none"></div>
            <div class="ruinOption ruinDoorLock doorlock-1" doorlock="1" data-transtitle="doorlock-locked"></div>
            <div class="ruinOption ruinDoorLock doorlock-2" doorlock="2" data-transtitle="doorlock-open"></div>
            <div class="ruinOption ruinDoorLock doorlock-3" doorlock="3" data-transtitle="doorlock-bottle"></div>
            <div class="ruinOption ruinDoorLock doorlock-4" doorlock="4" data-transtitle="doorlock-bump"></div>
            <div class="ruinOption ruinDoorLock doorlock-5" doorlock="5" data-transtitle="doorlock-magnet"></div>
            <div class="ruinOption ruinDoorLock doorlock-6" doorlock="6" data-transtitle="doorlock-stairs"></div>
        </div>
        <div id="ruinZombieBox">
            <h3 data-trans="zombies"></h3>
            <div class="ruinOption ruinZombie zombie-0" zombie="0" data-transtitle="zombies-none"></div>
            <div class="ruinOption ruinZombie zombie-1" zombie="1" data-transtitle="zombies-1"></div>
            <div class="ruinOption ruinZombie zombie-2" zombie="2" data-transtitle="zombies-2"></div>
            <div class="ruinOption ruinZombie zombie-3" zombie="3" data-transtitle="zombies-3"></div>
            <div class="ruinOption ruinZombie zombie-4" zombie="4" data-transtitle="zombies-4"></div>
        </div>
    </div>
    <div id="ruinInfoBox2">
        <h2 id="ruinName"></h2>
        <h4 id="ruinCoords">Floor: <span class="ruinFloor"></span> - X: <span class="ruinCoordsX"></span> - Y: <span
                class="ruinCoordsY"></span></h4>
        <div id="ruinZone"></div>
        <div id="ruinCommentBox">
            <h3 data-trans="comment"></h3>
            <textarea id="ruinComment"></textarea>
            <div id="ruinComment-save" data-trans="save"></div>
        </div>
        <div id="ruinHoverInfo"></div>

    </div>
    <div id="upperruinmap" class="clearfix"></div>
    <div id="lowerruinmap" class="clearfix"></div>
</div>
<div id="update-myzone">
    <button id="update-myzone-button" data-trans="update-myzone"></button>
</div>
<div id="box" style="display:none;" class="clearfix">
    <div id="box-protection" class="hideme"></div>
    <div id="box-content">
        <ul id="box-tabs" class="clearfix">
            <li id="tab-zone-info" ref="zone-info" class="box-tab active click" data-transtitle="zone"></li>
            <li id="tab-item-info" ref="item-info" class="box-tab click" data-transtitle="items"></li>
            <li id="tab-bank-info" ref="bank-info" class="box-tab click" data-transtitle="bank"></li>
            <li id="tab-town-info" ref="town-info" class="box-tab click" data-transtitle="city"></li>
            <li id="tab-citizens" ref="citizens" class="box-tab click" data-transtitle="citizens"></li>
            <li id="tab-storms" ref="storms" class="box-tab click" data-transtitle="desert-storms"></li>
            <li id="tab-ruins" ref="ruins" class="box-tab click" data-transtitle="ruins"></li>
            <li id="tab-expeditions" ref="expeditions" class="box-tab click" data-transtitle="expeditions"></li>
            <li id="tab-options" ref="options" class="box-tab click" data-transtitle="options"></li>
            <li id="tab-spread" class="box-spread click" data-transtitle="overview-expand">&lt;&gt;</li>
            <li id="tab-unspread" class="box-unspread click hideme" data-transtitle="overview-collapse">&gt;&lt;</li>
        </ul>

        <div id="zone-info" class="spread-no box-tab-content"></div>
        <div id="storms" class="spread-no box-tab-content hideme" data-trans="please-wait"></div>
        <div id="expeditions" class="spread-no box-tab-content hideme" data-trans="please-wait"></div>
        <div id="ruins" class="spread-me spread-4 box-tab-content hideme" data-trans="please-wait"></div>
        <div id="citizens" class="spread-me spread-3 box-tab-content hideme" data-trans="please-wait"></div>
        <div id="town-info" class="spread-me spread-5 box-tab-content hideme" data-trans="please-wait"></div>

        <div id="item-info" class="spread-me spread-1 hideme box-tab-content">
            <p class="desc" data-trans="item-info"></p>
            <div id="item-info-Rsc" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-rsc"></h3></div>
            <div id="item-info-Box" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-box"></h3></div>
            <div id="item-info-Furniture" class="zone-item-cat clearfix click" state="0"><h3
                    data-trans="item-furniture"></h3></div>
            <div id="item-info-Drug" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-drug"></h3>
            </div>
            <div id="item-info-Armor" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-armor"></h3>
            </div>
            <div id="item-info-Food" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-food"></h3>
            </div>
            <div id="item-info-Weapon" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-weapon"></h3>
            </div>
            <div id="item-info-Misc" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-misc"></h3>
            </div>
        </div>

        <div id="bank-info" class="spread-me spread-2 hideme box-tab-content">
            <p class="desc" data-trans="bank-info"></p>
            <div id="bank-info-Rsc" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-rsc"></h3></div>
            <div id="bank-info-Box" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-box"></h3></div>
            <div id="bank-info-Furniture" class="zone-item-cat clearfix click" state="0"><h3
                    data-trans="item-furniture"></h3></div>
            <div id="bank-info-Drug" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-drug"></h3>
            </div>
            <div id="bank-info-Armor" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-armor"></h3>
            </div>
            <div id="bank-info-Food" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-food"></h3>
            </div>
            <div id="bank-info-Weapon" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-weapon"></h3>
            </div>
            <div id="bank-info-Misc" class="zone-item-cat clearfix click" state="0"><h3 data-trans="item-misc"></h3>
            </div>
        </div>

        <div id="options" class="spread-no hideme box-tab-content">
            <div id="options-radius" class="options-section">
                <p class="desc" data-trans="options-radii"></p>
                <div id="colorpicker"></div>
                <input class="hideme" type="text" id="opt-radius-color" name="opt-radius-color" value="#9999ff"/>
                <input type="text" id="opt-radius-number" name="opt-radius-number" value="9" size="4"
                       maxlength="2"/><input type="radio" id="opt-radius-ap" name="opt-radius-metric" value="ap"
                                             checked="checked"/> <span data-trans="AP"></span>&nbsp;&nbsp;<input
                    type="radio" id="opt-radius-km" name="opt-radius-metric" value="km"/> <span data-trans="km"></span>&nbsp;&nbsp;<button
                    id="opt-radius-submit">Anzeigen
                </button>
                <br/><span class="hideme click interactive" onclick="removeRadius();" data-trans="delete-radii"></span>
                <div id="radius-list"></div>
                <div id="radius-save" class="click button" data-trans="save-radii"></div>
                <div id="radius-reset" class="click button" data-trans="reset-radii"></div>
            </div>
            <div id="options-display" class="options-section">
                <p class="desc" data-trans="options-display"></p>
                <div id="options-display-zonehover" class="click options-display-switch"
                     data-trans="display-hover"></div>
                <div id="options-display-geodir" class="click options-display-switch"
                     data-trans="display-compass"></div>
                <div id="options-display-driedzone" class="click options-display-option" ref="zone-status-dried"
                     data-trans="display-icon-dried"></div>
                <div id="options-display-fullzone" class="click options-display-option" ref="zone-status-full"
                     data-trans="display-icon-full"></div>
                <div id="options-display-stormzone" class="click options-display-option" ref="zone-storm-status"
                     data-trans="display-icon-storm"></div>
                <div id="options-display-citizens" class="click options-display-option" ref="citizen"
                     data-trans="display-citidot"></div>
                <div id="options-display-zombies" class="click options-display-option" ref="zombies"
                     data-trans="display-zombies"></div>
                <div id="options-display-uptodate" class="click options-display-option" ref="zone-updated"
                     data-trans="display-status"></div>
                <div id="options-save" class="click button" data-trans="save-settings"></div>
                <div id="options-reset" class="click button" data-trans="reset-settings"></div>
            </div>
            <div id="options-qrcode" class="options-section">
                <p class="desc"><span data-trans="options-bookmark1"></span> <a href="<?= $bookmark ?>"
                                                                                data-trans="bookmark"></a> <span
                        data-trans="options-bookmark2"></span></p>
                <p class="desc"><span data-trans="options-qr"></span><br><br><?= $qrcode ?></p>
                <div id="options-qrcode-img-wrapper" class=""></div>
            </div>
        </div>
    </div>
</div>
<div id="tools" class="clearfix hideme">
    <div id="tools-protection" class="hideme"></div>
    <div id="tools-content">
        <ul id="tools-tabs" class="clearfix">
            <li id="tab-tools-colors" ref="tools-colors" class="tools-tab active click" data-transtitle="palette"></li>
            <li id="tab-tools-options" ref="tools-options" class="hideme tools-tab click"
                data-transtitle="options"></li>
            <li id="tab-tools-planner" ref="tools-planner" class="tools-tab click" data-transtitle="planner"></li>
        </ul>

        <div id="tools-colors" class="tools-tab-content">
            <ul class="canvas-tools">
                <li><span data-trans="canvas"></span> <a class="interactive click" id="clear-canvasDraw"
                                                         data-trans="delete"></a><br><span data-trans="canvas"></span>
                    <a class="interactive click" id="save-canvasDraw" data-trans="save"></a></li>
            </ul>
            <ul class="canvas-tools">
                <li data-trans="pen-color"></li>
                <li class="draw-color click active" id="choosePurple" ref="#ff00ff" data-trans="purple"></li>
                <li class="draw-color click" id="chooseGreen" ref="#00ff00" data-trans="green"></li>
                <li class="draw-color click" id="chooseYellow" ref="#ffff00" data-trans="yellow"></li>
                <li class="draw-color click" id="chooseBrown" ref="#cc9900" data-trans="brown"></li>
            </ul>
            <ul class="canvas-tools">
                <li data-trans="pen-size"></li>
                <li class="draw-size click active" id="chooseS" ref="3" data-trans="small"></li>
                <li class="draw-size click" id="chooseM" ref="5" data-trans="normal"></li>
                <li class="draw-size click" id="chooseL" ref="8" data-trans="large"></li>
                <li class="draw-size click" id="chooseX" ref="12" data-trans="huge"></li>
            </ul>
        </div>

        <div id="tools-planner" class="hideme tools-tab-content">
            <ul class="canvas-tools">
                <li><span data-trans="canvas"></span> <a class="interactive click" id="clear-canvasPlan"
                                                         data-trans="delete"></a><br><span data-trans="canvas"></span>
                    <a class="interactive click" id="save-canvasPlan" data-trans="save"></a></li>
            </ul>
            <ul class="canvas-tools">
                <li>Route</li>
                <li class="draw-route click active" id="chooseR1" ref="1" data-trans="route-1"></li>
                <li class="draw-route click" id="chooseR2" ref="2" data-trans="route-2"></li>
                <li class="draw-route click" id="chooseR3" ref="3" data-trans="route-3"></li>
                <li class="draw-route click" id="chooseR4" ref="4" data-trans="route-4"></li>
                <li class="draw-route click" id="chooseR5" ref="5" data-trans="route-5"></li>
            </ul>
            <ul class="canvas-tools">
                <li><a class="interactive click" id="toggle-routeSave" data-trans="upload-route"></a></li>
                <li id="routeSave-form" class="hideme">
                    <span data-trans="route"></span> <input type="radio" name="routeSave-no" value="1"
                                                            checked="checked"/>1 <input type="radio" name="routeSave-no"
                                                                                        value="2"/>2 <input type="radio"
                                                                                                            name="routeSave-no"
                                                                                                            value="3"/>3
                    <input type="radio" name="routeSave-no" value="4"/>4 <input type="radio" name="routeSave-no"
                                                                                value="5"/>5<br/>
                    <span data-trans="name"></span> <input type="text" name="routeSave-name" size="20"
                                                           maxlength="20"/><br/>
                    <a class="interactive click plus" id="routeSave-save" data-trans="save-route"></a> <a
                        class="interactive click minus" id="routeSave-cancel" data-trans="cancel"></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="map-hover">
    <div id="map-hover-content">
        <div id="map-hover-coords"></div>
        <div id="map-hover-building"></div>
        <div id="map-hover-status"></div>
        <div id="map-hover-citizens"></div>
        <div id="map-hover-zombies"></div>
        <div id="map-hover-items"></div>
    </div>
</div>
<div id="item-selector" class="hideme">
    <div id="item-selector-Rsc" class="item-selector-cat clearfix"><h3 data-trans="item-rsc"></h3></div>
    <div id="item-selector-Box" class="item-selector-cat clearfix"><h3 data-trans="item-box"></h3></div>
    <div id="item-selector-Furniture" class="item-selector-cat clearfix"><h3 data-trans="item-furniture"></h3></div>
    <div id="item-selector-Drug" class="item-selector-cat clearfix"><h3 data-trans="item-drug"></h3></div>
    <div id="item-selector-Armor" class="item-selector-cat clearfix"><h3 data-trans="item-armor"></h3></div>
    <div id="item-selector-Food" class="item-selector-cat clearfix"><h3 data-trans="item-food"></h3></div>
    <div id="item-selector-Weapon" class="item-selector-cat clearfix"><h3 data-trans="item-weapon"></h3></div>
    <div id="item-selector-Misc" class="item-selector-cat clearfix"><h3 data-trans="item-misc"></h3></div>
</div>
<div id="building-selector" class="hideme"></div>
<script type="text/javascript">

    //var fm_url = 'https://fatamorgana.md26.eu/';
    // was data.fatamorgana.md26.eu previously. changed to subdomain because of issues.
    // was changes from hardcoded URL to auto-detect
    var fm_url = window.location.protocol + '//' + window.location.hostname + '/';

    var secureKey = "<?=$secureKey?>";
    var radiusCounter = 1;
    var mapHoverActive = false;
    var mapHoverMoved = false;
    var data = <?=$gamemap?>;
</script>
