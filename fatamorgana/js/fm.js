// translation service
(function () {
	var _dict = {};
	var _notFoundError = false;
	window.jsIn = {
		//  the format of DictInObject : {"key1":"text1", "key2":"text2",...}
		addDict: function (dict) {
			for (key in dict) {
				_dict[key] = dict[key];
			}
		},
		showNotFoundError: function (show) {
			if (typeof show !== 'undefined')
				_notFoundError = (show === true);

			return _notFoundError;
		},
		translate: function (text, param) {
			let newText = _dict[text] ||
				((!_notFoundError) ?
						text :
						(function () {
							throw "No entry found for key {" + text + "}";
						})()
				);

			//parse the place holders.
			if (param) {
				for (var i = 0, maxi = param.length; i < maxi; i++) {
					var regex = new RegExp('\{%' + (i + 1) + '\}', 'g');
					newText = newText.replace(regex, param[i]);
				}
			}
			return newText;
		}
	};
	window.__ = jsIn.translate;
})()

// fm code
function datetimeformat(timestamp) {
	var monat = new Array(__("Januar"), __("Februar"), __("März"), __("April"), __("Mai"), __("Juni"), __("Juli"), __("August"), __("September"), __("Oktober"), __("November"), __("Dezember"));

	var date = new Date(timestamp * 1000);

	var formats = {
		'de': __('am') + ' ' + date.getDate() + '. ' + monat[date.getMonth()] + ' ' + date.getFullYear() + ' ' + __('um') + ' ' + date.getHours() + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ' ' + __('Uhr') + ' ',
		'en': __('am') + ' ' + monat[date.getMonth()] + ' ' + date.getDate() + ((date.getDate() % 10 == 1 && date.getDate() != 11 ? 'st' : (date.getDate() % 10 == 2 && date.getDate() != 12 ? 'nd' : (date.getDate() % 10 == 3 && date.getDate() != 13 ? 'rd' : 'th')))) + ', ' + date.getFullYear() + ' ' + __('um') + ' ' + date.getHours() + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ' ',
		'fr': __('am') + ' ' + date.getDate() + ' ' + monat[date.getMonth()] + ' ' + date.getFullYear() + ' ' + __('um') + ' ' + date.getHours() + 'H' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ' ',
		'es': __('am') + ' ' + date.getDate() + ' de ' + monat[date.getMonth()] + ' del ' + date.getFullYear() + ' ' + __('um') + ' ' + date.getHours() + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ' ',
	}

	return formats[data.langcode];
}

function updateMapRulers(mapZone) {
	var rx = mapZone.attr('rx');
	var ry = mapZone.attr('ry');
	$('#map .mapruler').removeClass('hoverzone');
	$('#map .mapruler.ruler_y' + ry + ',#map .mapruler.ruler_x' + rx).addClass('hoverzone');
}

function updateLowerRuinMapRulers(mapZone) {
	var rx = mapZone.attr('rx');
	var ry = mapZone.attr('ry');
	$('#lowerruinmap .mapruler').removeClass('hoverzone');
	$('#lowerruinmap .mapruler.ruler_y' + ry + ',#lowerruinmap .mapruler.ruler_x' + rx).addClass('hoverzone');
}

function updateUpperRuinMapRulers(mapZone) {
	var rx = mapZone.attr('rx');
	var ry = mapZone.attr('ry');
	$('#upperruinmap .mapruler').removeClass('hoverzone');
	$('#upperruinmap .mapruler.ruler_y' + ry + ',#upperruinmap .mapruler.ruler_x' + rx).addClass('hoverzone');
}

function updateFloor(floor) {
	$('#ruinCoords .ruinFloor').text(floor);
}

function selectZone(mapZone) {
	$('#item-selector').addClass('hideme');
	$('#building-selector').addClass('hideme');
	var rx = mapZone.attr('rx');
	var ry = mapZone.attr('ry');
	$('#map .mapzone, #map .mapruler').removeClass('selectedZone');
	$('#map #x' + rx + 'y' + ry + ',#map .mapruler.ruler_y' + ry + ',#map .mapruler.ruler_x' + rx).addClass('selectedZone');
	var ax = mapZone.attr('ax');
	var ay = mapZone.attr('ay');
	updateBox(ax, ay, rx, ry);

	$('li.box-tab').removeClass('active');
	$('li.box-tab[ref="zone-info"]').addClass('active')
	$('div.box-tab-content').addClass('hideme');
	$('div#zone-info').removeClass('hideme');
}

function updateBox(x, y, rx, ry) {
	var zone;
	rx = parseInt(rx);
	ry = parseInt(ry);
	$('#item-selector').addClass('hideme');
	delete data.saveItems;
	var infoBox = $('#box-content #zone-info');
	if (!(rx === 0 && ry === 0)) {
		infoBox.html('<h3>' + __('Zone') + ' [' + rx + '|' + ry + ']</h3>');
		infoBox.append('<p>' + __('Entfernung') + ': ' + calcAP(rx, ry) + __('AP') + ', ' + calcKM(rx, ry) + __('km') + '</p>');
	} else {
		infoBox.html('<h3>' + __('Stadt') + ' [' + rx + '|' + ry + ']</h3>');

		infoBox.append('<p class="water">' + __('water') + ':<br>' + data.water.well + ' ' + __('in_well') + '<br>' + data.water.bank + ' ' + __('in_bank') + '</p>');

		if (data.door === 1) {
			infoBox.append('<p class="minus">' + __('gate') + ': ' + __('open') + '</p>');
		}
		else {
			infoBox.append('<p class="plus">' + __('gate') + ': ' + __('closed') + '</p>');
		}

	}
	if (data.map['y' + y] !== undefined && data.map['y' + y]['x' + x] !== undefined) {
		if (zone = data.map['y' + y]['x' + x]) {
			// building
			if (zone.building !== undefined) {
				if (!isExplorable(zone.building.type) && zone.building.type !== -1) {
					if (zone.building.dried !== undefined && zone.building.dried === 1) {
						if (data.spy === undefined) {
							infoBox.append('<p class="zone-building"><strong>' + zone.building.name + '</strong><br/><span class="minus">' + __('Gebäude ist leer.') + '</span> <a class="interactive plus ajaxlink" href="/update/building/regenerate" id="BUILDING-REGENERATE" ocx="' + x + '" ocy="' + y + '">' + __('regenerieren') + '</a></p>');
						} else {
							infoBox.append('<p class="zone-building"><strong>' + zone.building.name + '</strong><br/><span class="minus">' + __('Gebäude ist leer.') + '</span></p>');
						}
					} else {
						if (data.spy === undefined) {
							infoBox.append('<p class="zone-building"><strong>' + zone.building.name + '</strong><br/><span class="plus">' + __('Gebäude ist durchsuchbar.') + '</span> <a class="interactive minus ajaxlink" href="/update/building/deplete" id="BUILDING-DEPLETE" ocx="' + x + '" ocy="' + y + '">' + __('leeren') + '</a></p>');
						} else {
							infoBox.append('<p class="zone-building"><strong>' + zone.building.name + '</strong><br/><span class="plus">' + __('Gebäude ist durchsuchbar.') + '</span></p>');
						}
					}
					if (zone.building.blueprint !== undefined && zone.building.blueprint === 1) {
						if (data.spy === undefined) {
							infoBox.append('<p class="zone-building"><span class="minus">' + __('Blaupause wurde gefunden.') + '</span> <a class="interactive plus ajaxlink" href="/update/blueprint/available" id="BLUEPRINT-AVAILABLE" ocx="' + x + '" ocy="' + y + '">' + __('ist noch erhältlich') + '</a></p>');
						} else {
							infoBox.append('<p class="zone-building"><span class="minus">' + __('Blaupause wurde gefunden.') + '</span></p>');
						}
					} else {
						if (data.spy === undefined) {
							infoBox.append('<p class="zone-building"><span class="plus">' + __('Blaupause ist noch erhältlich.') + '</span> <a class="interactive minus ajaxlink" href="/update/blueprint/found" id="BLUEPRINT-FOUND" ocx="' + x + '" ocy="' + y + '">' + __('bereits gefunden') + '</a></p>');
						} else {
							infoBox.append('<p class="zone-building"><span class="plus">' + __('Blaupause ist noch erhältlich.') + '</span></p>');
						}
					}
				} else if (isExplorable(zone.building.type)) {
					infoBox.append('<p class="zone-building"><strong>' + zone.building.name + '</strong><br/><span class="">' + __('e-ruin') + ':</span> <a class="interactive explore" href="#" id="ENTER-BUILDING" ocx="' + x + '" ocy="' + y + '">' + __('enter-e-ruin') + '</a></p>');
				} else {
					infoBox.append('<p class="zone-building"><strong>' + zone.building.name + '</strong> <span class="minus">' + [zone.building.dig] + ' <img src="/css/img/h_dig.gif" /></span><br/>');
					if (zone.building.guess !== undefined) {
						infoBox.append(__('Aufklärer') + ': ' + data.buildings[zone.building.guess][data.langcode]);
						infoBox.append(' <a class="toggle-building-update interactive" href="#" id="GUESS-BUILDING" ocx="' + x + '" ocy="' + y + '">' + __('Vermutung ändern') + '</a>');
					} else {
						infoBox.append('<a class="toggle-building-update interactive" href="#" id="GUESS-BUILDING" ocx="' + x + '" ocy="' + y + '">' + __('Vermutung angeben') + '</a>');
					}
					infoBox.append('</p>');
				}
			}
			// regeneration
			if (zone.dried !== undefined && zone.dried === 1) {
				if (data.spy === undefined) {
					infoBox.append('<p class="zone-status zone-status-empty"><img src="/css/img/tag_5.gif" /> <span class="minus">' + __('Zone ist leer.') + '</span> <a class="interactive plus ajaxlink" href="/update/zone/regenerate" id="ZONE-REGENERATE" ocx="' + x + '" ocy="' + y + '">' + __('regenerieren') + '</a></p>');
				} else {
					infoBox.append('<p class="zone-status zone-status-empty"><img src="/css/img/tag_5.gif" /> <span class="minus">' + __('Zone ist leer.') + '</span></p>');
				}

				if (zone.updatedOn !== undefined && (data.system.gametype === 'uhu' || (data.system.gametype === 'far' && calcKM(rx, ry) >= 3))) {
					if (data.stormstamp !== undefined) {
						var stormcounter = 0;
						for (var i = 1; i <= data.system.day; i++) {
							if (data.stormstamp[i] !== undefined) {
								if (data.stormstamp[i] > zone.updatedOn) {
									if (calcGD(rx, ry) === data.stormnames[data.storm[i] - 1]) {
										stormcounter++;
									}
								}
							}
						}
						if (stormcounter > 0) {
							infoBox.append('<p class="zone-status zone-storm-status hideme"><img src="/css/img/storm.gif" /> <span class="">' + stormcounter + ' ' + (stormcounter === 1 ? __('storm') : __('storms')) + ' ' + __('since-last-update') + '</span></p>');
						}
					}
				}

			} else if (!(rx === 0 && ry === 0)) {
				if (data.spy === undefined) {
					infoBox.append('<p class="zone-status zone-status-full"><img src="/css/img/small_gather.gif" /> <span class="plus">' + __('Zone ist regeneriert.') + '</span> <a class="interactive minus ajaxlink" href="/update/zone/deplete" id="ZONE-DEPLETE" ocx="' + x + '" ocy="' + y + '">' + __('leeren') + '</a></p>');
				} else {
					infoBox.append('<p class="zone-status zone-status-full"><img src="/css/img/small_gather.gif" /> <span class="plus">' + __('Zone ist regeneriert.') + '</span></p>');
				}
			}
			// zombies
			if (!(rx === 0 && ry === 0)) {
				var diff = 0, days = 0;
				if (zone.updatedOn !== undefined && zone.nvt !== 0) {
					var date = new Date(zone.updatedOn * 1000);
					var dateN = new Date();
					dateN.setHours(23);
					dateN.setMinutes(59);
					dateN.setSeconds(59);
					if (date.getDate() !== dateN.getDate) {
						days = Math.floor((dateN.getTime() - date.getTime()) / 86400000);
						diff = days;
						if (zone.building !== undefined) {
							diff = 2 * days;
						}
					}
				}
				// Temporary disabling zombie estimation.
				diff = 0;
				if (data.spy === undefined) {
					infoBox.append('<p class="zone-zombies"><span class="hideme zombie-count-change plus">◄&nbsp;</span><span id="zombie-count-display">' + (zone.z !== undefined ? parseInt(zone.z) + diff : diff) + '</span><span class="hideme zombie-count-change minus">&nbsp;►</span> ' + __('Zombie') + (zone.z && zone.z === 1 ? '' : 's') + ' <a class="toggle-zombie-update interactive" href="/update/zombies">' + __('aktualisieren') + '</a><a class="hideme interactive ajaxlink" href="/update/zombies" id="UPDATE-ZOMBIES" ocx="' + x + '" ocy="' + y + '">' + __('speichern') + '</a></p>');
					if (days > 0 && zone.z !== undefined) {
						if (days === 1 && zone.z === 1) {
							infoBox.append('<p class="zone-zombies-scout"><em>' + __('Vor 1 Tag war es ein Zombie.') + '</em></p>');
						} else if (days === 1 && zone.z > 1) {
							infoBox.append('<p class="zone-zombies-scout"><em>' + __('Vor 1 Tag waren es {%1} Zombies.', [zone.z]) + '</em></p>');
						} else if (days > 1 && zone.z === 1) {
							infoBox.append('<p class="zone-zombies-scout"><em>' + __('Vor {%1} Tagen war es 1 Zombie.', [days]) + '</em></p>');
						} else if (days > 1 && zone.z > 1) {
							infoBox.append('<p class="zone-zombies-scout"><em>' + __('Vor {%1} Tagen waren es {%2} Zombies.', [days, zone.z]) + '</em></p>');
						}
					} else if (days > 0 && zone.z === undefined) {
						if (days === 1) {
							infoBox.append('<p class="zone-zombies-scout"><em>' + __('Vor 1 Tag waren es 0 Zombies.') + '</em></p>');
						} else if (days > 1) {
							infoBox.append('<p class="zone-zombies-scout"><em>' + __('Vor {%1} Tagen waren es 0 Zombies.', [days]) + '</em></p>');
						}
					}
					infoBox.append('<div class="hideme"><input type="hidden" value="' + (zone.z ? parseInt(zone.z) + diff : diff) + '" id="zombie-count-input" /></div>');
				} else {
					infoBox.append('<p class="zone-zombies"><span id="zombie-count-display">' + (zone.z ? zone.z : 0) + '</span> ' + __('Zombies') + '</p>');
				}

				if (data.spy === undefined) {
					infoBox.append('<p class="zone-maxzombies">' + __('Maximal') + ' <span class="hideme maxzombie-count-change plus">◄&nbsp;</span><span id="maxzombie-count-display">' + (zone.zm !== -1 ? parseInt(zone.zm) : 0) + '</span><span class="hideme maxzombie-count-change minus">&nbsp;►</span> ' + __('Zombie') + (zone.zm && zone.zm === 1 ? '' : 's') + ' ' + __('heute') + ' <a class="toggle-maxzombie-update interactive" href="/update/maxzombies">' + __('aktualisieren') + '</a><div class="hideme maxzombie-notice">' + __('maxzombiesnotice') + '</div><a class="hideme interactive ajaxlink" href="/update/maxzombies" id="UPDATE-MAXZOMBIES" ocx="' + x + '" ocy="' + y + '">' + __('speichern') + '</a></p>');
					infoBox.append('<div class="hideme"><input type="hidden" value="' + (zone.zm ? parseInt(zone.zm) : 0) + '" id="maxzombie-count-input" /></div>');
				} else if (zone.zm > 0) {
					infoBox.append('<p class="zone-maxzombies">' + __('Maximal') + ' <span id="maxzombie-count-display">' + zone.zm + '</span> ' + __('Zombie') + (zone.zm && zone.zm === 1 ? '' : 's') + ' ' + __('heute') + '</p>');
				}
			}

			if (data.system.chaos === 1) {
				if (data.spy === undefined ) {
					infoBox.append('<p class="zone-chaos-citizen"><img src="/css/img/small_arma.gif" /><img src="/css/img/small_human.gif" /> <span class="minus">' + __('CHAOS') + ':</span> <a class="interactive plus ajaxlink" href="/update/citizen" id="CITIZEN-LOCATION" ocx="' + x + '" ocy="' + y + '">' + __('Ich bin HIER!') + '</a></p>');
				}
			}

			// citizens
			if (zone['citizens'] !== undefined) {
				var cc = 0, ci = 0;
				var clist = '';
				var ilist = '';
				for (cid in zone['citizens']) {
					cc++;
					//if ( clist !== '' ) { clist += ', '; }
					if (rx === 0 && ry === 0 && data['citizens'][cid]['out'] === 0) {
						ci++;
						ilist += '<span class="zone-citizen zone-citizen-' + zone['citizens'][cid]['job'] + '">' + zone['citizens'][cid]['name'] + '</span> ';
					}
					else {
						clist += '<span class="zone-citizen zone-citizen-' + zone['citizens'][cid]['job'] + '">' + zone['citizens'][cid]['name'] + '</span> ';
					}
				}
				if (ci > 0) {
					infoBox.append('<p class="city-citizens">' + ci + ' ' + __('Bürger') + ' (' + __('inside') + '): ' + ilist + '</p>');
				}
				if (cc > ci) {
					infoBox.append('<p class="zone-citizens">' + (cc - ci) + ' ' + __('Bürger') + ((rx === 0 && ry === 0) ? ' (' + __('atgate') + ')' : '') + ': ' + clist + '</p>');
				}
			}

			// lost souls
			if (!(rx === 0 && ry === 0)) {
				if (zone['lostsoul'] === undefined || zone['lostsoul'] === 0) {
					if (data.spy === undefined) {
						infoBox.append('<p class="zone-soul zone-soul-0">' + __('Keine verirrte Seele') + ' <a class="interactive plus ajaxlink" href="/update/zone/addsoul" id="ZONE-ADDSOUL" ocx="' + x + '" ocy="' + y + '">' + __('hinzufügen') + '</a></p>');
					} else {
						infoBox.append('<p class="zone-soul zone-soul-0">' + __('Keine verirrte Seele') + '</p>');
					}
				} else {
					if (data.spy === undefined) {
						infoBox.append('<p class="zone-soul zone-soul-1"><img src="/css/img/tag_12.gif" /> <span class="plus">' + __('Hier spukt es.') + '</span> <a class="interactive minus ajaxlink" href="/update/zone/delsoul" id="ZONE-DELSOUL" ocx="' + x + '" ocy="' + y + '">' + __('Seele entfernen') + '</a></p>');
					} else {
						infoBox.append('<p class="zone-soul zone-soul-1"><img src="/css/img/tag_12.gif" /> <span class="plus">' + __('Hier spukt es..') + '</span></p>');
					}
				}
			}

			// items
			if (!(rx === 0 && ry === 0)) {
				if (data.spy === undefined) {
					infoBox.append('<p class="zone-items-header"><span>' + __('Gegenstände') + '</span> <a class="toggle-item-update interactive" href="/update/items" id="ZONE-ITEMS" ocx="' + x + '" ocy="' + y + '">' + __('aktualisieren') + '</a></p>');
				} else {
					infoBox.append('<p class="zone-items-header"><span>' + __('Gegenstände') + '</span></p>');
				}
				infoBox.append('<div id="zi_x' + rx + '_y' + ry + '" class="zone-items clearfix"></div>');
				if (zone['items'] !== undefined && !(rx === 0 && ry === 0)) {
					for (i in zone.items) {
						var item = zone.items[i];
						$('#zi_x' + rx + '_y' + ry).append(createItemDisplay(item.id, item.count, item.broken));
					}
				}
				if (data.spy === undefined) {
					infoBox.append('<p class="zone-items-footer hideme"><a class="close-item-selector minus interactive" href="close">' + __('schließen') + '</a>&nbsp;&nbsp;&nbsp;<a class="ajaxsave plus interactive" href="/update/items" id="ZONE-ITEMS" ocx="' + x + '" ocy="' + y + '">' + __('speichern') + '</a></p>');
				}
			}

			// update status
			if (!(rx === 0 && ry === 0)) {
				var updText = __('Letzte Aktualisierung') + ' ';
				if (zone.updatedOn !== undefined) {
					updText += datetimeformat(zone.updatedOn);
				}
				if (zone.updatedBy !== undefined && zone.updatedBy !== '') {
					updText += __('durch') + ' ' + zone.updatedBy;
				}
				infoBox.append('<p class="zone-lastupdate">' + updText + '</p>');
			}
		}
	} else {
		// zombies scout
		if (data.spy === undefined) {
			if (data.scout !== undefined && data.scout['y' + y + 'x' + x] !== undefined) {
				var zom = data.scout['y' + y + 'x' + x]['zom'];
				var pbl = data.scout['y' + y + 'x' + x]['pbl'];
				var upo = data.scout['y' + y + 'x' + x]['updatedOn'];
				var upb = data.scout['y' + y + 'x' + x]['updatedBy'];
			} else {
				var zom = 0;
				var pbl = 0;
				var upo = 0;
				var upb = 0;
			}
			infoBox.append('<p class="zone-zombies"><span class="hideme zombie-count-change plus">◄&nbsp;</span><span id="zombie-count-display">' + (zom ? zom : 0) + '</span><span class="hideme zombie-count-change minus">&nbsp;►</span> ' + __('Zombies') + ' <a class="toggle-zombie-update interactive" href="/update/scoutzombies">' + __('aktualisieren') + '</a><a class="hideme interactive ajaxlink" href="/update/scoutzombies" id="UPDATE-SCOUTZOMBIES" ocx="' + x + '" ocy="' + y + '">' + __('speichern') + '</a></p>');
			infoBox.append('<div class="hideme"><input type="hidden" value="' + (zom ? zom : 0) + '" id="zombie-count-input" /></div>');
			// possible building?
			if (pbl === 0) {
				if (data.spy === undefined) {
					infoBox.append('<p class="zone-status"><img src="/css/img/tag_5.gif" /> <span class="minus">' + __('Gebäude unwahrscheinlich') + '</span> <a class="interactive plus ajaxlink" href="/update/zone/buildingprobable" id="BUILDING-PROBABLE" ocx="' + x + '" ocy="' + y + '">' + __('vermutlich doch') + '</a></p>');
				}
			} else {
				if (data.spy === undefined) {
					infoBox.append('<p class="zone-status"><img src="/css/img/small_gather.gif" /> <span class="plus">' + __('vermutlich ein Gebäude') + '</span> <a class="interactive minus ajaxlink" href="/update/zone/buildingnotprobable" id="BUILDING-NOTPROBABLE" ocx="' + x + '" ocy="' + y + '">' + __('eher nicht') + '</a></p>');
				}
			}

			// lost souls
			if (!(rx === 0 && ry === 0)) {
				if (data.spy === undefined) {
					infoBox.append('<p class="zone-soul zone-soul-0">' + __('Keine verirrte Seele') + ' <a class="interactive plus ajaxlink" href="/update/zone/addsoul" id="ZONE-ADDSOUL" ocx="' + x + '" ocy="' + y + '">' + __('hinzufügen') + '</a></p>');
				} else {
					infoBox.append('<p class="zone-soul zone-soul-0">' + __('Keine verirrte Seele') + '</p>');
				}
			}

			// update status
			if (upo !== 0 && upb !== 0) {
				var updText = __('Letzte Aktualisierung') + ' ';
				updText += datetimeformat(upo);
				updText += __('durch') + ' ' + upb;
				infoBox.append('<p class="zone-lastupdate">' + updText + '</p>');
			}
		}
	}
}

function selectLowerRuinZone(mapZone) {
	var rx = mapZone.attr('rx');
	var ry = mapZone.attr('ry');
	data.crx = rx;
	data.cry = ry;
	$('#lowerruinmap .mapzone, #lowerruinmap .mapruler').removeClass('selectedZone');
	$('#lowerruinmap #x' + rx + 'y' + ry + ',#lowerruinmap .mapruler.ruler_y' + ry + ',#lowerruinmap .mapruler.ruler_x' + rx).addClass('selectedZone');
	var ax = mapZone.attr('ax');
	var ay = mapZone.attr('ay');
	$('#ruinZone').html(__('Ausgewähltes Gebäudefeld:') + ' ' + '<span id="ruinCX">' + rx + '</span>' + '|' + '<span id="ruinCY">' + ry + '</span>');
	$('#ruinComment').val(mapZone.attr('comment'));
}

function selectUpperRuinZone(mapZone) {
	var rx = mapZone.attr('rx');
	var ry = mapZone.attr('ry');
	data.crx = rx;
	data.cry = ry;
	$('#upperruinmap .mapzone, #upperruinmap .mapruler').removeClass('selectedZone');
	$('#upperruinmap #x' + rx + 'y' + ry + ',#upperruinmap .mapruler.ruler_y' + ry + ',#upperruinmap .mapruler.ruler_x' + rx).addClass('selectedZone');
	var ax = mapZone.attr('ax');
	var ay = mapZone.attr('ay');
	$('#ruinZone').html(__('Ausgewähltes Gebäudefeld:') + ' ' + '<span id="ruinCX">' + rx + '</span>' + '|' + '<span id="ruinCY">' + ry + '</span>');
	$('#ruinComment').val(mapZone.attr('comment'));
}

function createItemDisplay(id, count, broken) {
	if (data.items[id] !== undefined) {
		var raw_item = data.items[id];
		var classBroken = broken === 1 ? ' broken' : (id < 1 ? ' broken' : '');
		var classDef = raw_item.category === 'Armor' ? ' defense' : '';
		return '<div class="zone-item click' + classBroken + classDef + '" state="0" ref="' + raw_item.id + '" count="' + count + '"><img src="' + data.system.icon + raw_item.image + '" title="' + raw_item.name[data.langcode] + ' (ID: ' + Math.abs(id) + ')" />&nbsp;<span class="count">' + count + '</span></div>';
	} else {
		return '';
	}
}

function createItemDisplaySmall(id) {
	var raw_item = data.items[id];
	var classBroken = id < 1 ? ' broken' : '';
	var classDef = raw_item.category === 'Armor' ? ' defense' : '';
	return '<div class="select-item click' + classBroken + classDef + '" ref="' + raw_item.id + '"><img src="' + data.system.icon + raw_item.image + '" title="' + raw_item.name[data.langcode] + ' (ID: ' + Math.abs(id) + ')" /></div>';

}

function createBuildingDisplaySmall(id) {
	var name = data.buildings[id][data.langcode];
	return '<div class="select-building click" ref="' + id + '">' + name + '</div>';
}

function calcAP(x, y) {
	return Math.abs(x) + Math.abs(y);
}

function calcKM(x, y) {
	return Math.round(Math.sqrt(x * x + y * y));
}

function calcGD(x, y) {
	if (x === 0 && y === 0) {
		return false;
	}
	if (x > Math.floor(y / 2) && y > Math.floor(x / 2)) {
		return 'NE';
	}
	if (x > Math.floor(-y / 2) && -y > Math.floor(x / 2)) {
		return 'SE';
	}
	if (-x > Math.floor(y / 2) && y > Math.floor(-x / 2)) {
		return 'NW';
	}
	if (-x > Math.floor(-y / 2) && -y > Math.floor(-x / 2)) {
		return 'SW';
	}
	if (Math.abs(x) > Math.abs(y)) {
		return (x > 0) ? 'E' : 'W';
	}
	return (y > 0) ? 'N' : 'S';
}

function calcGDN(x, y) {
	if (x === 0 && y === 0) {
		return false;
	}
	if (x > Math.floor(y / 2) && y > Math.floor(x / 2)) {
		return 1;
	}
	if (x > Math.floor(-y / 2) && -y > Math.floor(x / 2)) {
		return 3;
	}
	if (-x > Math.floor(y / 2) && y > Math.floor(-x / 2)) {
		return 7;
	}
	if (-x > Math.floor(-y / 2) && -y > Math.floor(-x / 2)) {
		return 5;
	}
	if (Math.abs(x) > Math.abs(y)) {
		return (x > 0) ? 2 : 6;
	}
	return (y > 0) ? 0 : 4;
}

function highlightZoneWithItem(zoneitem) {
	var itemid = parseInt(zoneitem.attr('ref'));
	if (zoneitem.hasClass('broken')) {
		itemid *= -1;
	}
	for (i = 0; i < data['height']; i++) {
		for (j = 0; j < data['width']; j++) {
			var mx = 'x' + j;
			var my = 'y' + i;
			var rx = j - data['tx'];
			var ry = data['ty'] - i;
			if (data.map[my] !== undefined && data.map[my][mx] !== undefined) {
				if (data.map[my][mx]['items'] !== undefined && data.map[my][mx]['items'].length > 0 && !(rx === 0 && ry === 0)) {
					var zoneItems = data.map[my][mx]['items'];
					for (zi in zoneItems) {
						var zitem = zoneItems[zi];
						if (zitem.id === itemid) {
							highlightZone(rx, ry);
						}
					}
				}
			}
		}
	}
}

function highlightZonesWithItem() {
	$('.mapzone').removeClass('highlight');
	$('div.zone-item[state="1"]').each(function () {
		highlightZoneWithItem($(this));
	});
}

function highlightZone(x, y) {
	$('#x' + x + 'y' + y).addClass('highlight');
}

function downdarkZone(x, y) {
	$('#x' + x + 'y' + y).removeClass('highlight');
}

function highlightSpecialZone(x, y) {
	$('#x' + x + 'y' + y).addClass('highlightSpecial');
}

function downdarkSpecialZone(x, y) {
	$('#x' + x + 'y' + y).removeClass('highlightSpecial');
}

function addRadius(range, metric, color, name = false) {
	$('#dynascript').append('<style type="text/css">li.mapzone.highlight-' + radiusCounter + '-border-n { border-top-color: ' + color + '; } li.mapzone.highlight-' + radiusCounter + '-border-e { border-right-color: ' + color + '; } li.mapzone.highlight-' + radiusCounter + '-border-s { border-bottom-color: ' + color + '; } li.mapzone.highlight-' + radiusCounter + '-border-w { border-left-color: ' + color + '; }</style>');
	$('.mapzone[' + metric + '="' + range + '"]').each(function (e) {
		var change = $(this).attr(metric + 'c');
		for (c = 0; c < change.length; c++) {
			$(this).addClass('highlight-radius').addClass('highlight-' + radiusCounter + '-border-' + change.charAt(c));
		}
	});
	if (!name) {
		name = __('Radius') + ' ' + radiusCounter;
	}
	$('#radius-list').append('<div class="radius-list-item radius-delete hideme click" data-range="'+range+'" data-metric="'+metric+'" data-color="'+color+'" data-name="'+name+'" id="radius-delete-' + radiusCounter + '" onclick="removeRadius(' + radiusCounter + ');"><div class="radius-color-example" style="background-color:' + color + ';"></div><span>' + __('löschen') + '</span>' + name + ': ' + range + ' ' + metric + '</div>');
	$('#radius-delete-' + radiusCounter).slideDown(750);

	radiusCounter++;
}

function removeAllRadius() {
	$('.mapzone.highlight-border-n').removeClass('highlight-border-n').css('border-color', 'rgba(0,0,0,.2)');
	$('.mapzone.highlight-border-e').removeClass('highlight-border-e').css('border-color', 'rgba(0,0,0,.2)');
	$('.mapzone.highlight-border-s').removeClass('highlight-border-s').css('border-color', 'rgba(0,0,0,.2)');
	$('.mapzone.highlight-border-w').removeClass('highlight-border-w').css('border-color', 'rgba(0,0,0,.2)');
}

function removeRadius(rid) {
	$('.mapzone.highlight-' + rid + '-border-n').removeClass('highlight-' + rid + '-border-n');
	$('.mapzone.highlight-' + rid + '-border-e').removeClass('highlight-' + rid + '-border-e');
	$('.mapzone.highlight-' + rid + '-border-s').removeClass('highlight-' + rid + '-border-s');
	$('.mapzone.highlight-' + rid + '-border-w').removeClass('highlight-' + rid + '-border-w');
	$('#radius-delete-' + rid).slideUp(750);
}

function walkInTheDesert(e) {

	if (data.cx === undefined || data.cy === undefined) { // current coords not set
		return;
	}
	if ($('#zone-info').hasClass('hideme')) { // zone info tab not active
		return;
	}
	if (e.key === "ArrowLeft" || e.key === "ArrowRight" || e.key === "ArrowUp" || e.key === "ArrowDown") {
		e.preventDefault();
		var initClick = false;
		if (e.key === 'ArrowLeft' && data.cx > -(data.tx)) {
			data.cx -= 1;
			initClick = true;
		}
		if (e.key === 'ArrowUp' && data.cy < data.ty) {
			data.cy = parseInt(data.cy) + 1;
			initClick = true;
		}
		if (e.key === 'ArrowRight' && data.cx < data.width - data.tx - 1) {
			data.cx = parseInt(data.cx) + 1;
			initClick = true;
		}
		if (e.key === 'ArrowDown' && data.cy > data.ty - data.height + 1) {
			data.cy -= 1;
			initClick = true;
		}
		if (initClick) {
			$('#map #x' + (data.cx) + 'y' + (data.cy)).click();
		}
	}
}

function walkInTheRuin(e) {

	if (data.crx === undefined || data.cry === undefined) { // current coords not set
		data.crx = 0;
		data.cry = 1;
	}
	if (e.key === "ArrowLeft" || e.key === "ArrowRight" || e.key === "ArrowUp" || e.key === "ArrowDown") {
		e.preventDefault();
		var initClick = false;
		if (e.key === 'ArrowLeft' && data.crx > -7) {
			data.crx -= 1;
			initClick = true;
		}
		if (e.key === 'ArrowDown' && data.cry < 13) {
			data.cry = parseInt(data.cry) + 1;
			initClick = true;
		}
		if (e.key === 'ArrowRight' && data.crx < 5) {
			data.crx = parseInt(data.crx) + 1;
			initClick = true;
		}
		if (e.key === 'ArrowUp' && data.cry > 1) {
			data.cry -= 1;
			initClick = true;
		}
		if (initClick) {
			$('#lowerruinmap #x' + (data.crx) + 'y' + (data.cry)).click();
		}
	}
}

function moveMapHover(e) {
	var mapHover = $("#map-hover");
	var offset = 18;
	var x = e.pageX + offset;
	var y = e.pageY + offset;
	if (y > $(window).height() - mapHover.height()) {
		y -= mapHover.height() + offset * 2;
		if (y < offset * 2) {
			y = $(window).height() / 2 - mapHover.height() / 2;
		}
	}
	mapHover.css({'left': x, 'top': y});
	mapHoverMoved = true;
}

function hideMapHover() {
	$("#map-hover").hide();
	mapHoverMoved = false;
}

function showMapHover(e, z) {
	if (!mapHoverMoved) {
		moveMapHover(e);
	}
	var selectedZone = $('#' + z);
	if (selectedZone.attr('rx') === '0' && selectedZone.attr('ry') === '0') {
		return;
	}
	$("#map-hover").html(fillMapHover(selectedZone));
	$("#map-hover").show();
	mapHoverMoved = false;
}

function fillMapHover(z) {
	var rx = z.attr('rx');
	var ry = z.attr('ry');
	var ax = z.attr('ax');
	var ay = z.attr('ay');

	$('#map-hover-coords').html('<strong>[' + rx + '|' + ry + ']</strong> - ' + calcAP(rx, ry) + ' AP / ' + calcKM(rx, ry) + ' km');
	if (data.map['y' + ay] !== undefined && data.map['y' + ay]['x' + ax] !== undefined) {
		if (data.map['y' + ay]['x' + ax]['building'] !== undefined) {
			$('#map-hover-building').html('<strong>' + data.map['y' + ay]['x' + ax]['building']['name'] + '</strong>');
		} else {
			$('#map-hover-building').html(' ');
		}
		if (data.map['y' + ay]['x' + ax]['dried'] !== undefined) {
			if (data.map['y' + ay]['x' + ax]['dried'] === 1) {
				$('#map-hover-status').html('<strong class="minus">' + __('Zone ist leer.') + '</strong>');
			} else {
				$('#map-hover-status').html('<strong class="plus">' + __('Zone ist buddelbar.') + '</strong>');
			}
		} else {
			$('#map-hover-status').html(' ');
		}
		if (data.map['y' + ay]['x' + ax]['citizens'] !== undefined) {
			var cc = 0;
			var clist = '';
			var zone = data.map['y' + ay]['x' + ax];
			for (cid in zone['citizens']) {
				cc++;
				//if ( clist !== '' ) { clist += ', '; }
				clist += '<span class="zone-citizen zone-citizen-' + zone['citizens'][cid]['job'] + '">' + zone['citizens'][cid]['name'] + '</span> ';
			}
			if (cc > 0) {
				//infoBox.append('<p class="zone-citizens">'+ cc +' Bürger: '+ clist +'</p>');
				$('#map-hover-citizens').html(clist);
			} else {
				$('#map-hover-citizens').html(' ');
			}
		} else {
			$('#map-hover-citizens').html(' ');
		}
		if (data.map['y' + ay]['x' + ax]['xmlz'] !== undefined && data.map['y' + ay]['x' + ax]['xmlz'] !== null) {
			var zcnt = parseInt(data.map['y' + ay]['x' + ax]['xmlz']);
			if (zcnt === 1) {
				$('#map-hover-zombies').html('<strong class="minus">1 ' + __('Zombie') + '</strong>');
			} else {
				$('#map-hover-zombies').html('<strong class="minus">' + zcnt + ' ' + __('Zombies') + '</strong>');
			}
		}
		else if (data.map['y' + ay]['x' + ax]['z'] !== undefined && data.map['y' + ay]['x' + ax]['nvt'] === 0) {
			var zcnt = parseInt(data.map['y' + ay]['x' + ax]['z']);
			if (zcnt === 1) {
				$('#map-hover-zombies').html('<strong class="minus">1 ' + __('Zombie') + '</strong>');
			} else {
				$('#map-hover-zombies').html('<strong class="minus">' + zcnt + ' ' + __('Zombies') + '</strong>');
			}
		}
		else if (data.map['y' + ay]['x' + ax]['z'] !== undefined) {
			var zone = data.map['y' + ay]['x' + ax];
			var diff = 0, days = 0;
			if (zone.updatedOn !== undefined && zone.nvt !== 0) {
				var date = new Date(zone.updatedOn * 1000);
				var dateN = new Date();
				dateN.setHours(23);
				dateN.setMinutes(59);
				dateN.setSeconds(59);
				if (date.getDate() !== dateN.getDate) {
					days = Math.floor((dateN.getTime() - date.getTime()) / 86400000);
					diff = days;
					if (zone.building !== undefined) {
						diff = 2 * days;
					}
				}
			}
			diff = 0;
			var zest = '';
			if (days > 0) {
				if (days === 1 && zone.z === 1) {
					zest = __('Vor 1 Tag war es ein Zombie.');
				} else if (days === 1 && zone.z > 1) {
					zest = __('Vor 1 Tag waren es {%1} Zombies.', [zone.z]);
				} else if (days > 1 && zone.z === 1) {
					zest = __('Vor {%1} Tagen war es ein Zombie.', [days]);
				} else if (days > 1 && zone.z > 1) {
					zest = __('Vor {%1} Tagen waren es {%2} Zombies.', [days, zone.z]);
				}
				//zest = '<br/><em style="color:#ccc;">' + zest + '</em>';
				$('#map-hover-zombies').html('<strong class="minus">' + zest + '</strong>');
			}
			else if (zone.updatedOn === undefined) {
				if (zone.z === 1) {
					zest = __('Irgendwann wurde 1 Zombie gesichtet.');
				}
				else {
					zest = __('Irgendwann wurden {%1} Zombies gesichtet.', [zone.z]);
				}
				$('#map-hover-zombies').html('<strong class="minus">' + zest + '</strong>');
			}
			else {
				var zcnt = parseInt(data.map['y' + ay]['x' + ax]['z']) + diff;
				if (zcnt === 1) {
					$('#map-hover-zombies').html('<strong class="minus">1 ' + __('Zombie') + '</strong>');
				} else {
					$('#map-hover-zombies').html('<strong class="minus">' + zcnt + ' ' + __('Zombies') + '</strong>');
				}
			}

		} else if (data.map['y' + ay]['x' + ax]['updatedOn'] !== undefined) {
			var zone = data.map['y' + ay]['x' + ax];
			var diff = 0, days = 0;
			var date = new Date(zone.updatedOn * 1000);
			var dateN = new Date();
			dateN.setHours(23);
			dateN.setMinutes(59);
			dateN.setSeconds(59);
			if (date.getDate() !== dateN.getDate) {
				days = Math.floor((dateN.getTime() - date.getTime()) / 86400000);
				diff = days;
				if (zone.building !== undefined) {
					diff = 2 * days;
				}
			}
			diff = 0;
			var zest = '';
			if (days > 0) {
				if (days === 1) {
					zest = __('Vor 1 Tag waren es 0 Zombies.');
				} else if (days > 1) {
					zest = __('Vor {%1} Tagen waren es 0 Zombies.', [days]);
				}
				$('#map-hover-zombies').html('<strong class="minus">' + zest + '</strong>');
			}
			else {
				/*var zcnt = diff;

				if (zcnt === 1) {
					$('#map-hover-zombies').html('<strong class="minus">1 ' + __('Zombie') + '</strong>' + zest);
				} else {
					$('#map-hover-zombies').html('<strong class="minus">' + zcnt + ' ' + __('Zombies') + '</strong>' + zest);
				}*/
				$('#map-hover-zombies').html('<strong class="minus">' + '?? ' + __('Zombies') + '</strong>');
			}

		} else {
			$('#map-hover-zombies').html(' ');
		}
		if (data.map['y' + ay]['x' + ax]['items'] !== undefined && data.map['y' + ay]['x' + ax]['items'].length > 0) {
			var zone = data.map['y' + ay]['x' + ax];
			var itemBox = $('#map-hover-items');
			itemBox.html('<p><strong>' + __('Gegenstände auf dem Boden') + '</strong></p>');
			itemBox.append('<div id="zh_x' + rx + '_y' + ry + '" class="zone-items clearfix"></div>');
			for (i in zone.items) {
				var item = zone.items[i];
				$('#zh_x' + rx + '_y' + ry).append(createItemDisplay(item.id, item.count, item.broken));
			}
		} else {
			$('#map-hover-items').html(' ');
		}
	} else {
		$('#map-hover-building').html(' ');
		$('#map-hover-status').html(' ');
		$('#map-hover-citizens').html(' ');
		$('#map-hover-zombies').html(' ');
		$('#map-hover-items').html(' ');
		if (data.scout !== undefined && data.scout['y' + ay + 'x' + ax] !== undefined) {
			if (data.scout['y' + ay + 'x' + ax]['pbl'] === 1) {
				$('#map-hover-building').html('<strong>' + __('Hier ist wahrscheinlich ein Gebäude.') + '</strong>');
			}
			if (data.scout['y' + ay + 'x' + ax]['zom'] === 1) {
				$('#map-hover-zombies').html('<strong class="minus">' + __('vermutlich 1 Zombie') + '</strong>');
			} else if (data.scout['y' + ay + 'x' + ax]['zom'] > 1) {
				$('#map-hover-zombies').html('<strong class="minus">' + __('vermutlich {%1} Zombies', [data.scout['y' + ay + 'x' + ax]['zom']]) + '</strong>');
			}
		}
	}
}

function ajaxMyzoneUpdate() {
	if (window.softreloads >= 10) {
		location.reload();
	}
	else {
		window.softreloads++;
		var token = secureKey;
		protectBox(true);
		$.ajax({
						 type: "POST",
						 url: "/map/updatemyzone",
						 data: "key=" + token,
						 success: function (msg) {
							 $('#dynascript').append(msg);
							 protectBox(false);
						 }
					 });
	}
}

function ajaxUpdate(ec, z, zm) {
	var el = $('#' + ec);
	var token = secureKey;
	var ocX = el.attr('ocx');
	var ocY = el.attr('ocy');
	var ocAction = ec;
	protectBox(true);
	$.ajax({
		type: "POST",
		url: "/map/update",
		data: "key=" + token + "&action=" + ocAction + "&x=" + ocX + "&y=" + ocY + "&z=" + z + "&zm=" + zm,
		success: function (msg) {
			$('#dynascript').append(msg);
			protectBox(false);
		}
	});
}

function ajaxInfo(msg) {
	var responseItem = $(document.createElement('p')).addClass('ajaxInfo').addClass('hideme').html(msg);
	$('#userInfoBox').append(responseItem);
	responseItem.hide().removeClass('hideme').slideDown(250).delay(2500).slideUp(500);
}

function ajaxRuinUpdate(ocAction, z) {
	var token = secureKey;
	var ocX = $('.ruinCoordsX').html();
	var ocY = $('.ruinCoordsY').html();
	var floor = $('.ruinFloor').html();
	var riX = parseInt($('#ruinCX').html());
	var riY = parseInt($('#ruinCY').html());
	if (riY > 0 || floor == 'lower') {
		var ocAD = riX + '|' + riY + '|' + z;
		/*protectRuinBox(true);*/
		$.ajax({
			type: "POST",
			url: "/map/update",
			data: "key=" + token + "&action=" + ocAction + "&x=" + ocX + "&y=" + ocY + "&z=" + ocAD + "&floor=" + floor,
			success: function (msg) {
				$('#dynascript').append(msg);
				/*protectRuinBox(false);*/
			}
		});
	} else {
		ajaxInfo(__('Bitte gültiges Feld wählen.'));
	}
}

function generateMapZone(i, j) {
	var ax = j;
	var ay = i;
	var mx = 'x' + j;
	var my = 'y' + i;
	var rx = j - data['tx'];
	var ry = data['ty'] - i;
	var rzd = null;

	var mapzone = $(document.createElement('li'));

	mapzone.addClass('mapzone').attr('rx', rx).attr('ry', ry).attr('ax', ax).attr('ay', ay);
	mapzone.attr('id', 'x' + rx + 'y' + ry);
	if (data.ox === j && data.oy === i) {
		mapzone.addClass('selectedZone');
	}

	var ap = calcAP(rx, ry);
	var km = calcKM(rx, ry);
	var gd = 'gd-' + calcGD(rx, ry);
	mapzone.attr('ap', ap).attr('km', km).addClass(gd);

	var apc = '';
	var kmc = '';
	if (ax > 0 && rx <= 0) { // west borders
		var aap = calcAP(rx - 1, ry);
		var akm = calcKM(rx - 1, ry);
		if (aap !== ap) {
			apc += 'w';
		}
		if (akm !== km) {
			kmc += 'w';
		}
	}
	if (ax < data.width - 1 && rx >= 0) { // east borders
		var aap = calcAP(rx + 1, ry);
		var akm = calcKM(rx + 1, ry);
		if (aap !== ap) {
			apc += 'e';
		}
		if (akm !== km) {
			kmc += 'e';
		}
	}
	if (ay < data.height - 1 && ry <= 0) { // south borders
		var aap = calcAP(rx, ry - 1);
		var akm = calcKM(rx, ry - 1);
		if (aap !== ap) {
			apc += 's';
		}
		if (akm !== km) {
			kmc += 's';
		}
	}
	if (ay > 0 && ry >= 0) { // north borders
		var aap = calcAP(rx, ry + 1);
		var akm = calcKM(rx, ry + 1);
		if (aap !== ap) {
			apc += 'n';
		}
		if (akm !== km) {
			kmc += 'n';
		}
	}
	mapzone.attr('apc', apc).attr('kmc', kmc);

	if (data.map[my] !== undefined && data.map[my][mx] !== undefined) {

		if (data.map[my][mx]['items'] !== undefined && data.map[my][mx]['items'].length > 0 && !(rx === 0 && ry === 0)) {
			var zoneItems = data.map[my][mx]['items'];
			for (zi in zoneItems) {
				var zitem = zoneItems[zi];
				if (data.items[zitem.id] !== undefined) {
					var ritem = data.items[zitem.id];
					if (data.mapitems[ritem.category][ritem.id] !== undefined) {
						data.mapitems[ritem.category][ritem.id] += zitem.count;
					} else {
						data.mapitems[ritem.category][ritem.id] = zitem.count;
					}
				}
			}
		}

		var diff = 0, days = 0;
		var zc = null;
		var zone = data.map[my][mx];
		if (zone.updatedOn !== undefined) {
			var date = new Date(zone.updatedOn * 1000);
			var dateN = new Date();
			dateN.setHours(23);
			dateN.setMinutes(59);
			dateN.setSeconds(59);
			if (date.getDate() !== dateN.getDate) {
				days = Math.floor((dateN.getTime() - date.getTime()) / 86400000);
				diff = days;
				if (zone.building !== undefined) {
					diff = 2 * days;
				}
			}
		}
		// Temporarily disabling zombie estimation.
		diff = 0;

		if (data.map[my][mx]['z'] !== undefined) {
			var zc = parseInt(data.map[my][mx]['z']) + diff;
			var zl = parseInt(data.map[my][mx]['z']);
		} else {
			var zc = diff;
			var zl = 0;
		}

		if (data.map[my][mx]['z'] !== undefined) {
			mapzone.attr('z', data.map[my][mx]['z']);
		}
		if (data.map[my][mx]['xmlz'] !== undefined) {
			mapzone.attr('xmlz', data.map[my][mx]['xmlz']);
		}

		if (data.map[my][mx]['danger'] !== undefined && data.map[my][mx]['danger'] !== null) {
			mapzone.addClass('danger' + data.map[my][mx]['danger']);
			rzd = data.map[my][mx]['danger'];
		} else if (zc !== null) {
			if (zc > 0) {
				mapzone.attr('e', zc);
			}
			if (zl === 0) {
				mapzone.addClass('danger0');
			} else if (zl === 1) {
				mapzone.addClass('danger1');
				rzd = 1;
			} else if (zl >= 2 && zl <= 4) {
				mapzone.addClass('danger2');
				rzd = 2;
			} else if (zl >= 5 && zl <= 8) {
				mapzone.addClass('danger3');
				rzd = 3;
			} else if (zl > 8) {
				mapzone.addClass('danger4');
				rzd = 4
			} else {
				mapzone.addClass('danger0');
			}
		} else {
			mapzone.addClass('danger0');
		}
		if (data.map[my][mx]['tag'] !== undefined && data.map[my][mx]['tag'] !== null) {
			mapzone.addClass('tag' + data.map[my][mx]['tag']);
		}
		if (data.map[my][mx]['nvt'] !== undefined && data.map[my][mx]['nvt'] === 1) {
			mapzone.addClass('nvt');
		}
		if (data.map[my][mx]['building'] !== undefined && !(rx === 0 && ry === 0)) {
			var building = $(document.createElement('div')).addClass('building');
			if (!isExplorable(data.map[my][mx]['building']['type'])) {
				if (data.map[my][mx]['building']['dried'] === 1) {
					building.addClass('depleted-building');
				}
				if (data.map[my][mx]['building']['blueprint'] === undefined || data.map[my][mx]['building']['blueprint'] === 0) {
					building.addClass('building-blueprint');
					if (km >= 10) {
						building.addClass('building-blueprint-far');
					}
				}
			} else {
				building.addClass('explorable-building');
			}
			mapzone.append(building);
		}
		if (data.map[my][mx]['lostsoul'] !== undefined && data.map[my][mx]['lostsoul'] === 1 && !(rx === 0 && ry === 0)) {
			mapzone.addClass('lostsoul');
			var lostsoul = $(document.createElement('div')).addClass('lostsoul');
			mapzone.append(lostsoul);
		}
		if (data.map[my][mx]['dried'] !== undefined && !(rx === 0 && ry === 0)) {
			var zsi = $(document.createElement('img')).addClass('zone-status-img');
			if (data.map[my][mx]['dried'] === 1) {
				zsi.attr('src', '/css/img/tag_5.gif').addClass('zone-status-dried');
				if (!document.getElementById('options-display-driedzone').classList.contains('active-option')) {
					zsi.addClass('hideme');
				}
			} else {
				zsi.attr('src', '/css/img/small_gather.gif').addClass('zone-status-full');
				if (!document.getElementById('options-display-fullzone').classList.contains('active-option')) {
					zsi.addClass('hideme');
				}
			}
			mapzone.append(zsi);

			if (data.map[my][mx]['dried'] === 1) {
				if (data.map[my][mx]['updatedOn'] !== undefined && (data.system.gametype === 'uhu' || (data.system.gametype === 'far' && calcKM(rx, ry) >= 3))) {
					if (data.stormstamp !== undefined) {
						var stormcounter = 0;
						for (var i = 1; i <= data.system.day; i++) {
							if (data.stormstamp[i] !== undefined) {
								if (data.stormstamp[i] > data.map[my][mx]['updatedOn']) {
									if (calcGD(rx, ry) === data.stormnames[data.storm[i] - 1]) {
										stormcounter++;
									}
								}
							}
						}
						if (stormcounter > 0) {
							var zss = $(document.createElement('div')).addClass('zone-storm-status');
							zss.title = stormcounter + ' ' + (stormcounter === 1 ? __('storm') : __('storms')) + ' ' + __('since-last-update');
							zss.html(stormcounter);
							if (!document.getElementById('options-display-stormzone').classList.contains('active-option')) {
								zss.addClass('hideme');
							}
							mapzone.append(zss);
						}
					}
				}
			}
		}
		if (data.map[my][mx]['updatedOn'] !== undefined && !(rx === 0 && ry === 0)) {
			var udate = new Date(data.map[my][mx]['updatedOn'] * 1000);
			var cdate = new Date();
			var ydate = new Date();
			ydate.setDate(ydate.getDate() - 1);
			var bdate = new Date();
			bdate.setDate(bdate.getDate() - 2);
			if (udate.getDate() === cdate.getDate() && udate.getMonth() === cdate.getMonth()) {
				var utd = $(document.createElement('div')).addClass('zone-updated').addClass('zone-updated-today').attr('title', __('Heute aktualisiert'));
			} else if (udate.getDate() === ydate.getDate() && udate.getMonth() === ydate.getMonth()) {
				var utd = $(document.createElement('div')).addClass('zone-updated').addClass('zone-updated-yesterday').attr('title', __('Gestern aktualisiert'));
			} else if (udate.getDate() === bdate.getDate() && udate.getMonth() === bdate.getMonth()) {
				var utd = $(document.createElement('div')).addClass('zone-updated').addClass('zone-updated-b4yesterday').attr('title', __('Vorgestern aktualisiert'));
			} else {
				var utd = $(document.createElement('div')).addClass('zone-updated').addClass('zone-updated-longago').attr('title', __('Irgendwann mal aktualisiert'));
			}
			if (!document.getElementById('options-display-uptodate').classList.contains('active-option')) {
				utd.addClass('hideme');
			}
			mapzone.append(utd);
		}
		if (data.map[my][mx]['citizens'] !== undefined && !(rx === 0 && ry === 0)) {
			var cc = 0;
			for (cid in data.map[my][mx]['citizens']) {
				cc++;
			}
			if (cc > 0) {
				var citidot = $(document.createElement('div')).addClass('citizen');
				if (!document.getElementById('options-display-citizens').classList.contains('active-option')) {
					citidot.addClass('hideme');
				}
				citidot.attr('style', 'background: transparent url("' + fm_url + 'img/citidots.php?cs=2&cc=' + cc + '") no-repeat;');
				mapzone.append(citidot);
			}
		}
		if (data.map[my][mx]['xmlz'] !== undefined && data.map[my][mx]['xmlz'] > 0 && !(rx === 0 && ry === 0)) {
			var zombdot = $(document.createElement('div')).addClass('zombies').addClass('zombie-exact');
			zombdot.html('<span>' + data.map[my][mx]['xmlz'] + '</span>');
			if (!document.getElementById('options-display-zombies').classList.contains('active-option')) {
				zombdot.addClass('hideme');
			}
			mapzone.append(zombdot);
		}
		else if (data.map[my][mx]['z'] !== undefined && data.map[my][mx]['z'] > 0 && !(rx === 0 && ry === 0) && data.map[my][mx]['nvt'] === 0) {
			var zombdot = $(document.createElement('div')).addClass('zombies').addClass('zombie-exact');
			zombdot.html('<span>' + data.map[my][mx]['z'] + '</span>');
			if (!document.getElementById('options-display-zombies').classList.contains('active-option')) {
				zombdot.addClass('hideme');
			}
			mapzone.append(zombdot);
		}
		else if (rzd > 0) {
			var zombdot = $(document.createElement('div')).addClass('zombies');
			zombdot.attr('style', 'background: transparent url("' + fm_url + 'img/citidots.php?cs=1&cc=' + rzd + '") no-repeat;');
			if (!document.getElementById('options-display-zombies').classList.contains('active-option')) {
				zombdot.addClass('hideme');
			}
			mapzone.append(zombdot);
		}
	} else {
		mapzone.addClass('nyv');
		if (data.scout !== undefined && data.scout[my + mx] !== undefined) {
			if (data.scout[my + mx]['pbl'] === 1) {
				var building = $(document.createElement('div')).addClass('possible-building');
				mapzone.append(building);
			}
		}
	}
	if (rx === 0 && ry === 0) {
		mapzone.addClass('city');
	}

	return mapzone;

}

function reMoveCitizen(id) {
	for (i = 0; i < data['height']; i++) {
		for (j = 0; j < data['width']; j++) {
			var ax = j;
			var ay = i;
			var mx = 'x' + j;
			var my = 'y' + i;
			var rx = j - data['tx'];
			var ry = data['ty'] - i;
			var rzd = null;
			if (data.map[my] !== undefined && data.map[my][mx] !== undefined && data.map[my][mx]['citizens'] !== undefined && data.map[my][mx]['citizens'][id] !== undefined) {
				delete data.map[my][mx]['citizens'][id];
				$('#x' + rx + 'y' + ry).replaceWith(generateMapZone(ay, ax));
			}
		}
	}
}

function populateItemSelector() {
	for (i in data.items) {
		$('#item-selector-' + data.items[i]['category']).append(createItemDisplaySmall(i));
	}
}

function populateBuildingSelector() {
	for (i in data.buildings) {
		$('#building-selector').append(createBuildingDisplaySmall(i));
	}
}

function zoneItemList(x, y) {
	this.x = x;
	this.y = y;
}

function saveZoneItems(x, y, serial) {
	protectBox(true);
	$.ajax({
		type: "POST",
		url: "/map/update",
		data: "key=" + secureKey + "&action=ZONE-ITEMS&x=" + (parseInt(x) + parseInt(data.tx)) + "&y=" + (parseInt(data.ty) - parseInt(y)) + "&z=" + serial,
		success: function (msg) {
			//alert(msg);
			$('#dynascript').append(msg);
			protectBox(false);
		}
	});
}

function saveBuildingGuess(x, y, bID) {
	protectBox(true);
	$.ajax({
		type: "POST",
		url: "/map/update",
		data: "key=" + secureKey + "&action=BUILDING-GUESS&x=" + (parseInt(x) + parseInt(data.tx)) + "&y=" + (parseInt(data.ty) - parseInt(y)) + "&z=" + bID,
		success: function (msg) {
			//alert(msg);
			$('#dynascript').append(msg);
			protectBox(false);
		}
	});
}

function protectBox(state) {
	if (state) {
		$('#box-protection').css('height', $('#box').css('height'));
		$('#box-protection').css('width', $('#box').css('width'));
		$('#box-protection').removeClass('hideme');
	} else {
		$('#box-protection').addClass('hideme');
	}
}

function toggleGeoDirDisplay() {
	$('.mapzone').toggleClass('geodir');
}

function generateStormList() {
	if (data.storm === undefined) {
		return false;
	}

	var stormBox = $('#storms');
	var stormRoutes = generateStormRoutesList(parseInt(data.system.day));
	var stormList = $(document.createElement('ul')).addClass('storm-list');
	var stormForm = $(document.createElement('div')).addClass('storm-form');
	stormForm.append('<select id="storm-today" name="storm-today"><option value="0">' + __('keine Beobachtung') + '</option></select>').append('<a class="interactive ajaxupdate" href="/update/storm" id="UPDATE-STORM">' + __('speichern') + '</a>');
	stormBox.html('').append(stormForm).append(stormList);
	for (i = 1; i < 9; i++) {
		$('#storm-today').append('<option value="' + i + '">' + __(data.stormnames[i - 1]) + '</option>');
	}
	for (s in data.storm) {
		if (parseInt(s) === parseInt(data.system.day)) {
			$('#storm-today').val(s);
		}
		if (data.storm[s] > 0) {
			$('.storm-list').prepend('<li>' + __('Tag') + ' ' + s + ': <strong>' + __(data.stormnames[data.storm[s] - 1]) + '</strong></li>');
		} else {
			$('.storm-list').prepend('<li>' + __('Tag') + ' ' + s + ': <strong>' + __('keine Beobachtung') + '</strong></li>');
		}
	}
}

function generateStormRoutesList() {
	if (data.storm[data.system.day] === 0) {
		return false;
	}

	var stormRouteTemplates = {
		1: {
			"A": "X1YAx1y8",
			"B": "x1YAX1y8",
			"C": "Y6x1Y1x1Y1x1y2X1y2X1y2",
			"D": "Y6X1Y1X1Y1X1y2x1y2x1y2",
			"E": "X2Y9x1y7",
			"F": "x2Y9X1y7",
			"G": "YAX1y8",
			"H": "YAx1y8",
			"I": "Y5X1Y4X1y5x1y2",
			"J": "Y5x1Y4x1y5X1y2",
			"K": "x3Y8X1y4X1y2",
			"L": "X3Y8x1y4x1y2",
			"M": "Y6x1Y1x1Y1x1y2X1y2X1y2",
			"N": "Y6X1Y1X1Y1X1y2x1y2x1y2",
			"O": "X2Y9x1y7",
			"P": "x2Y9X1y7",
		},
		2: {
			"A": "Y2X2Y1X1Y2X1Y1X1y2x1y2x1y1x1",
			"B": "X2Y2X1Y1X2Y1X1Y1x2y1x2y1x1y1",
			"C": "X3Y3X1Y4x1y3x1y3",
			"D": "Y3X3Y1X4y1x3y1x3",
			"E": "X2Y2X2Y2X1Y2x1y1x1y2x2y1",
			"F": "Y2X2Y2X2Y1X2y1x1y1x2y2x1",
			"G": "X3Y3X1Y4x1y3x1y3",
			"H": "Y3X3Y1X4y1x3y1x3",
			"I": "Y2X2Y1X1Y2X1Y1X1y2x1y2x1y1x1",
			"J": "X2Y2X1Y1X2Y1X1Y1x2y1x2y1x1y1",
		},
		3: {
			"A": "y1XAY1x8",
			"B": "Y1XAy1x8",
			"C": "X6Y1X1Y1X1Y1x2y1x2y1x2",
			"D": "X6y1X1y1X1y1x2Y1x2Y1x2",
			"E": "y2X9Y1x7",
			"F": "Y2X9y1x7",
			"G": "XAy1x8",
			"H": "XAY1x8",
			"I": "X5y1X4y1x5Y1x2",
			"J": "X5Y1X4Y1x5y1x2",
			"K": "Y3X8y1x4y1x2",
			"L": "y3X8Y1x4Y1x2",
			"M": "X6Y1X1Y1X1Y1x2y1x2y1x2",
			"N": "X6y1X1y1X1y1x2Y1x2Y1x2",
			"O": "y2X9Y1x7",
			"P": "Y2X9y1x7",
		},
		4: {
			"A": "y2X2y1X1y2X1y1X1Y2x1Y2x1Y1x1",
			"B": "X2y2X1y1X2y1X1y1x2Y1x2Y1x1Y1",
			"C": "X3y3X1y4x1Y3x1Y3",
			"D": "y3X3y1X4Y1x3Y1x3",
			"E": "X2y2X2y2X1y2x1Y1x1Y2x2Y1",
			"F": "y2X2y2X2y1X2Y1x1Y1x2Y2x1",
			"G": "X3y3X1y4x1Y3x1Y3",
			"H": "y3X3y1X4Y1x3Y1x3",
			"I": "y2X2y1X1y2X1y1X1Y2x1Y2x1Y1x1",
			"J": "X2y2X1y1X2y1X1y1x2Y1x2Y1x1Y1",
		},
		5: {
			"A": "X1yAx1Y8",
			"B": "x1yAX1Y8",
			"C": "y6x1y1x1y1x1Y2X1Y2X1Y2",
			"D": "y6X1y1X1y1X1Y2x1Y2x1Y2",
			"E": "X2y9x1Y7",
			"F": "x2y9X1Y7",
			"G": "yAX1Y8",
			"H": "yAx1Y8",
			"I": "y5X1y4X1Y5x1Y2",
			"J": "y5x1y4x1Y5X1Y2",
			"K": "x3y8X1Y4X1Y2",
			"L": "X3y8x1Y4x1Y2",
			"M": "y6x1y1x1y1x1Y2X1Y2X1Y2",
			"N": "y6X1y1X1y1X1Y2x1Y2x1Y2",
			"O": "X2y9x1Y7",
			"P": "x2y9X1Y7",
		},
		6: {
			"A": "y2x2y1x1y2x1y1x1Y2X1Y2X1Y1X1",
			"B": "x2y2x1y1x2y1x1y1X2Y1X2Y1X1Y1",
			"C": "x3y3x1y4X1Y3X1Y3",
			"D": "y3x3y1x4Y1X3Y1X3",
			"E": "x2y2x2y2x1y2X1Y1X1Y2X2Y1",
			"F": "y2x2y2x2y1x2Y1X1Y1X2Y2X1",
			"G": "x3y3x1y4X1Y3X1Y3",
			"H": "y3x3y1x4Y1X3Y1X3",
			"I": "y2x2y1x1y2x1y1x1Y2X1Y2X1Y1X1",
			"J": "x2y2x1y1x2y1x1y1X2Y1X2Y1X1Y1",
		},
		7: {
			"A": "y1xAY1X8",
			"B": "Y1xAy1X8",
			"C": "x6Y1x1Y1x1Y1X2y1X2y1X2",
			"D": "x6y1x1y1x1y1X2Y1X2Y1X2",
			"E": "y2x9Y1X7",
			"F": "Y2x9y1X7",
			"G": "xAy1X8",
			"H": "xAY1X8",
			"I": "x5y1x4y1X5Y1X2",
			"J": "x5Y1x4Y1X5y1X2",
			"K": "Y3x8y1X4y1X2",
			"L": "y3x8Y1X4Y1X2",
			"M": "x6Y1x1Y1x1Y1X2y1X2y1X2",
			"N": "x6y1x1y1x1y1X2Y1X2Y1X2",
			"O": "y2x9Y1X7",
			"P": "Y2x9y1X7",
		},
		8: {
			"A": "Y2x2Y1x1Y2x1Y1x1y2X1y2X1y1X1",
			"B": "x2Y2x1Y1x2Y1x1Y1X2y1X2y1X1y1",
			"C": "x3Y3x1Y4X1y3X1y3",
			"D": "Y3x3Y1x4y1X3y1X3",
			"E": "x2Y2x2Y2x1Y2X1y1X1y2X2y1",
			"F": "Y2x2Y2x2Y1x2y1X1y1X2y2X1",
			"G": "x3Y3x1Y4X1y3X1y3",
			"H": "Y3x3Y1x4y1X3y1X3",
			"I": "Y2x2Y1x1Y2x1Y1x1y2X1y2X1y1X1",
			"J": "x2Y2x1Y1x2Y1x1Y1X2y1X2y1X1y1",
		},
	};

	var stormRoutes = {};
	var startX = parseInt(data.tx);
	var startY = parseInt(data.ty);

	var templates = stormRouteTemplates[data.storm[data.system.day]];
	for (var r in templates) {
		var wrx = startX;
		var wry = startY;
		var route = {
			//0: {"x": wrx, "y": wry}
		};
		var wrc = templates[r].match(/.{1,2}/g);
		var step = 0;
		for (var wri = 0; wri < wrc.length; wri++) {
			var move = wrc[wri];
			var factor = 0;
			if (move.substr(0, 1) === 'x' || move.substr(0, 1) === 'Y') {
				factor = -1;
			} else {
				factor = 1;
			}
			var distance = move.substr(1, 1);
			var steps = 0;
			if (distance === "A") {
				steps = 10;
			} else {
				steps = parseInt(distance);
			}
			for (var s = 1; s <= steps; s++) {
				if (move.substr(0, 1) === 'x' || move.substr(0, 1) === 'X') {
					wrx += factor;
				} else {
					wry += factor;
				}
				route[step] = {"x": wrx, "y": wry};
				step++;
			}
		}

		stormRoutes[r] = {
			"creator": 0,
			"day": data.system.day,
			"name": "<span style='color: #ff9;'>Windroute " + r + "</span>",
			"route": route,
		}
	}
	data.stormRoutes = stormRoutes;
	return stormRoutes;
}

function generateExpeditionList() {
	if (data.expeditions === undefined) {
		return false;
	}
	var expBox = $('#expeditions');
	var expList = $(document.createElement('ul')).addClass('exp-list');
	var stormRoutes = generateStormRoutesList();
	for (e in data.expeditions) {
		var expData = data.expeditions[e];
		var expEntry = $(document.createElement('li')).addClass('exp-item click').attr('id', e).html(expData.name);
		expList.prepend(expEntry);
		// expData.day + '.' + expData.creator
	}
	for (e in stormRoutes) {
		var expData = stormRoutes[e];
		var expEntry = $(document.createElement('li')).addClass('exp-item click').attr('id', e).html(expData.name);
		expList.prepend(expEntry);
		// expData.day + '.' + expData.creator
	}
	expList.prepend($(document.createElement('li')).addClass('exp-item click active-option').attr('id', '0.0').html(__('Anzeige ausblenden')));
	expBox.html('<p style="color:#c00;background-color:#ff9;">' + __('wind-routes-experimental') + '</p>').append(expList);
	//expBox.html('').append(expList);
}

function highlightRoute(r) {
	$('.mapzone').removeClass('highlightRoute');
	$('.route-counter').remove();
	if (r !== '0.0') {
		if (data.expeditions[r]) {
			var route = data.expeditions[r]['route'];
		} else {
			var route = data.stormRoutes[r]['route'];
		}
		var delayTimer = 100;
		var c = 0;
		for (p in route) {
			c++;
			//alert("highlightZone("+(parseInt(p.x) - parseInt(data.tx))+", "+(parseInt(data.ty) - parseInt(p.y))+");");
			//highlightZone((parseInt(route[p].x) - parseInt(data.tx)), (parseInt(data.ty) - parseInt(route[p].y)));
			var px = parseInt(route[p].x) - parseInt(data.tx);
			var py = parseInt(data.ty) - parseInt(route[p].y);
			setTimeout("highlightRouteZone(" + px + "," + py + "," + c + ")", delayTimer);
			delayTimer += 100;
		}
	}
}

function highlightRouteZone(x, y, c) {
	if (!(x === 0 && y === 0)) {
		if ($('#x' + x + 'y' + y + ' .route-counter').length > 0) {
			$('#x' + x + 'y' + y + ' .route-counter').html($('#x' + x + 'y' + y + ' .route-counter').addClass('route-counter-multi').html() + ':' + c);
		} else {
			var cntdiv = $(document.createElement('div')).addClass('route-counter').html(c);
			$('#x' + x + 'y' + y).addClass('highlightRoute').prepend(cntdiv);
		}
	}
}

function generateRuinList() {
	var awrBox = $('#ruins');
	var awrTable = $(document.createElement('table')).addClass('awr-table');
	var awr = {};
	var a = 0;
	var zones = 0;
	var ruins = 0;
	for (y in data.map) {
		var yrow = data.map[y];
		for (x in yrow) {
			zones++;
			var xcell = yrow[x];
			if (xcell.building !== undefined) {
				ruins++;
				var bdata = xcell.building;
				bdata['x'] = xcell.rx;
				bdata['y'] = xcell.ry;
				bdata['ap'] = calcAP(xcell.rx, xcell.ry);
				bdata['km'] = calcKM(xcell.rx, xcell.ry);
				if (bdata['blueprint'] === undefined) {
					bdata['blueprint'] = 0;
				}
				bdata['explorable'] = isExplorable(bdata['type']);
				var gdn = calcGDN(xcell['rx'], xcell['ry']);
				if (awr[gdn] === undefined) {
					awr[gdn] = {};
				}
				if (bdata['x'] !== undefined && bdata['y'] !== undefined) {
					awr[gdn][a] = bdata;
				}
				a++;
			}
		}
	}
	for (c in data.scout) {
		var xcell = data.scout[c];
		if (xcell.pbl !== undefined && xcell.pbl === 1) {
			var coord = c.split('y')[1].split('x');
			if (data.map['y' + coord[0]] === undefined || data.map['y' + coord[0]]['x' + coord[1]] === undefined) {
				var bdata = {};
				bdata['name'] = '<em style="color:#666;">' + __('Vermutetes Gebäude') + '</em>';
				bdata['x'] = coord[1] - data.tx;
				bdata['y'] = data.ty - coord[0];
				bdata['ap'] = calcAP(bdata['x'], bdata['y']);
				bdata['km'] = calcKM(bdata['x'], bdata['y']);
				bdata['blueprint'] = 0;
				var gdn = calcGDN(bdata['x'], bdata['y']);
				if (awr[gdn] === undefined) {
					awr[gdn] = {};
				}
				awr[gdn][a] = bdata;
				a++;
			}
		}
	}

	for (gdn in awr) {
		var stn = __(data.stormnames[gdn]);
		var stc = 0;
		var stt = '';
		for (sto in data.storm) {
			if (parseInt(data.storm[sto]) === (parseInt(gdn) + 1)) {
				stc++;
			}
		}
		if (stc > 0) {
			stt = ' <em>(' + stc + 'x ' + __('Sturm') + ')</em>';
		}
		var awrHeaderRow = $(document.createElement('tr')).addClass('awr-header').html('<th colspan="3">' + stn + stt + '</th>');
		awrTable.append(awrHeaderRow);
		var gdl = awr[gdn];
		for (i in gdl) {
			var b = gdl[i];
			var awrRow = $(document.createElement('tr')).addClass('awr-entry').html('<td class="pos-stat ' + (b.dried !== undefined && b.dried === 1 ? 'ruin-empty' : 'ruin-regen') + '"><abbr onmouseover="highlightSpecialZone(' + b.x + ',' + b.y + ');" onmouseout="downdarkSpecialZone(' + b.x + ',' + b.y + ');" onclick="$(\'#x' + (b.x) + 'y' + (b.y) + '\').click();" class="ruin-coords" title="' + b.ap + 'AP (' + b.km + 'km)">[' + b.x + '|' + b.y + ']</abbr></td><td>' + '<img src="/css/img/' + (b.explorable === true ? 'explore' : (b.blueprint !== undefined && b.blueprint === 1 ? 'no-bp' : (b.km >= 10 ? 'bp' : 'gp'))) + '.png">' + '</td><td>' + (b.name !== __('Ein nicht freigeschaufeltes Gebäude.') ? b.name : '<em style="color:#c00;"> ' + b.dig + ' ' + __('Haufen') + ' <img style="vertical-align:text-bottom;height:14px;" src="/css/img/h_dig.gif"></em>') + '</td>');
			awrTable.append(awrRow);
		}
	}
	awrBox.html('<p class="desc">' + __('{%1} ', [zones]) + __('von') + __(' {%1} ', [(data.height * data.width)]) + __('Zonen entdeckt') + __(' ({%1}%).', [Math.round(zones / data.height / data.width * 100)]) + '<br>' + __('{%1} ', [ruins]) + __('von') + ' 21 ' + __('Ruinen entdeckt') + __(' ({%1}%).', [Math.round(ruins / 21 * 100)]) + '</p>').append(awrTable);
}

function generateCitizenList() {
	var citCount = 0,
		liveCount = 0,
		deadCount = 0;
	if (data.citizens !== undefined) {
		let count = 0;
		for (let c in data.citizens) {
			count++;
		}
		citCount = count;
		liveCount = count;
	}
	if (data.cadavers !== undefined) {
		let count = 0;
		for (let d in data.cadavers) {
			if (d >= 0) {
				for (let c in data.cadavers[d]) {
					count++;
				}
			}
		}
		citCount = citCount + count;
		deadCount = count;
	}
	if (data.citizens !== undefined) {
		var citBox = $('#citizens');
		var citStats = $(document.createElement('p')).addClass('cit-stats').html(citCount + ' ' + __('Bürger') + ': ' + liveCount + ' ' + __('alive') + ' / ' + deadCount + ' ' + __('dead'));
		var citTable = $(document.createElement('table')).addClass('cit-table');
		var cit = {};

		for (cid in data.citizens) {
			let name = data.citizens[cid]['name'].trim().toUpperCase();
			cit[name] = cid;
		}
		let keys = Object.keys(cit),
			i, len = keys.length;

		keys.sort();
		for (i = 0; i < len; i++) {
			let k = keys[i];
			let cid = cit[k];
			var ctd = data.citizens[cid];
			if (data.system.chaos === 0) {
				var citRow = $(document.createElement('tr')).addClass('cit-entry').html('<td class="pos-stat"><abbr onmouseover="highlightSpecialZone(' + ctd.rx + ',' + ctd.ry + ');" onmouseout="downdarkSpecialZone(' + ctd.rx + ',' + ctd.ry + ');" onclick="$(\'#x' + (ctd.rx) + 'y' + (ctd.ry) + '\').click();" class="ruin-coords">[' + ctd.rx + '|' + ctd.ry + ']</abbr></td><td><span class="zone-citizen zone-citizen-' + ctd.job + '">' + ctd.name + '</span></td>');
			} else {
				var citRow = $(document.createElement('tr')).addClass('cit-entry').html('<td class="pos-stat"><abbr class="ruin-coords">[??|??]</abbr></td><td>' + ctd.name + '</td>');
			}
			citTable.append(citRow);
		}
		citBox.html('').append(citStats).append(citTable);
	}
	if (data.cadavers !== undefined) {
		var cadTable = $(document.createElement('table')).addClass('cad-table');

		for (var d = data.system.day; d >= 0; d--) {
			if (data.cadavers[d] !== undefined) {
				var cadRow = $(document.createElement('tr')).addClass('cad-day').html('<td colspan="2">' + __('Tag') + ' ' + d + '</td>');
				cadTable.append(cadRow);

				var cad = [];
				for (cid in data.cadavers[d]) {
					cad.push([cid, data.cadavers[d][cid]['name']]);
					reMoveCitizen(cid);
				}
				cad.sort(function (a, b) {
					return a[1] > b[1]
				});

				for (cco in cad) {
					var ctd = data.cadavers[d][cad[cco][0]];
					var cadRow = $(document.createElement('tr')).addClass('cad-entry').html('<td class="dtype dtype-' + ctd.dtype + '" title="' + __('Tod Typ ' + ctd.dtype) + '"></td><td ' + (ctd.rx !== undefined ? 'title="' + __('Letzter bekannter Aufenthaltsort') + ': [' + ctd.rx + '|' + ctd.ry + ']"' : '') + '>' + ctd.name + '</td>');
					cadTable.append(cadRow);
				}
			}
		}
		citBox.append(cadTable);
	}
}

function generateConstructionList() {
	var conBox = $('#town-info');
	conBox.html('');
	if (data.constructions !== undefined && Object.keys(data.constructions).length > 0 && data.mconstructions !== undefined && data.mconstructions.length > 0) {
		var conHelpBox = $(document.createElement('p')).addClass('desc').html(__('Die XML-Daten sind aktuell, aber nicht sehr informativ. Die manuell übertragenen Daten haben höheren Informationswert, können aber nur von Hand an die FM gesendet werden.'));
		var switchXML = $(document.createElement('a')).addClass('constructions-xml interactive click').html('XML');
		var switchMAN = $(document.createElement('a')).addClass('constructions-man interactive click').html('Manuell');
		conHelpBox.prepend(switchMAN).prepend(switchXML);
		conBox.append(conHelpBox);
	}
	var con = {};
	var a = 0;
	if (data.constructions !== undefined && Object.keys(data.constructions).length > 0) {
		var conTable = $(document.createElement('table')).addClass('construction-table construction-table-xml');
		var conHeaderRow = $(document.createElement('tr')).addClass('construction-header').html('<th colspan="2">' + __('Konstruktionen') + '</th>');
		conTable.append(conHeaderRow);
		for (c in data.constructions) {
			var concon = data.constructions[c];
			if (concon.level !== undefined) {
				var level = ' (' + concon.level + ')';
			} else {
				var level = '';
			}
			var conRow = $(document.createElement('tr')).addClass('con-entry').html('<td class="con-icon' + (concon.temp === 1 ? ' temporary' : '') + '"><img src="https://myhordes.eu/build/images/' + concon.image + '" /></td><td>' + concon.name[data.langcode] + level + '</td>');
			conTable.append(conRow);
		}
		conBox.append(conTable);
	}
	if (data.mconstructions !== undefined && data.mconstructions.length > 0) {
		var conTable = $(document.createElement('table')).addClass('construction-table construction-table-man');
		if (data.constructions !== undefined && Object.keys(data.constructions).length > 0) {
			conTable.addClass('hideme');
		}
		if (data.mconststamp !== undefined) {
			var conUpdateRow = $(document.createElement('tr')).addClass('construction-update').html('<td colspan="3">' + __('Stand: ') + data.mconststamp + ' ' + __('Uhr') + '</td>');
			conTable.append(conUpdateRow);
		}
		var conHeaderRow = $(document.createElement('tr')).addClass('construction-header').html('<th colspan="2">' + __('Konstruktion') + '</th><th>' + __('???') + '</th>');
		conTable.append(conHeaderRow);
		var mconid = 0;
		for (c in data.mconstructions) {
			mconid++;
			var concon = data.mconstructions[c];
			if (concon.level !== undefined) {
				var level = ' (' + concon.level + ')';
			} else {
				var level = '';
			}
			var conRow = $(document.createElement('tr')).addClass('mcon-entry').addClass((concon.done === 1 ? 'mcon-done' : (concon.lock === 1 ? 'mcon-lock' : 'mcon-active'))).addClass((mconid % 2 === 1 ? 'odd' : 'even')).html('<td class="con-icon' + '">' + (concon.indent !== undefined && concon.indent > 1 ? '<img src="/css/img/small_parent.gif" />' : '') + (concon.indent !== undefined && concon.indent > 0 ? '<img src="/css/img/small_parent.gif" />' : '') + '<img src="' + concon.image + '" /></td><td class="' + (concon.indent !== undefined && concon.indent === 0 ? 'item' : 'subitem') + '">' + concon.name[data.langcode] + level + '</td><td>' + (concon.done === 1 ? __('✓') : (concon.lock === 1 ? __('x') : '<abbr title="' + __(' AP') + '">' + concon.ap + '</abbr>')) + '</td>');
			conTable.append(conRow);
		}
		conBox.append(conTable);
	}
}

function isExplorable(buildingID) {
	var eba = new Array(61, 62, 63);
	var length = eba.length;
	for (var i = 0; i < length; i++) {
		if (eba[i] === buildingID) return true;
	}
	return false;
}

function generateRuinMap(x, y) {
	// Upper Floor
	$('#upperruinmap').html('');
	for (i = 0; i < 14; i++) {
		var maprow = $(document.createElement('ul')).addClass('maprow');
		for (j = 0; j < 13; j++) {
			var ax = j;
			var ay = i;
			var mx = 'x' + j;
			var my = 'y' + i;
			var rx = j - 8;
			var ry = i;
			var rzd = null;

			var px = 'x' + x;
			var py = 'y' + y;
			maprow.append(generateRuinMapZone(i, j, data.map[py][px]['building']['ruin'], 'upper'));
		}
		var maprulerF = $(document.createElement('li')).addClass('mapruler').addClass('ruler_y' + ry).addClass('first').html(ry);
		var maprulerL = maprulerF.clone().removeClass('first').addClass('last');
		maprow.prepend(maprulerF);
		maprow.append(maprulerL);
		$('#upperruinmap').append(maprow);
	}
	var maprulebarT = $(document.createElement('ul')).addClass('maprow').addClass('maprulebar').addClass('maprulebar-top');
	for (j = 0; j < 13; j++) {
		var maprulex = $(document.createElement('li')).addClass('mapruler').addClass('ruler_x' + (j - 7)).html(j - 7);
		maprulebarT.append(maprulex);
	}
	var mapruleoF = $(document.createElement('li')).addClass('mapcorner').addClass('first');
	var mapruleoL = mapruleoF.clone().removeClass('first').addClass('last');
	maprulebarT.prepend(mapruleoF);
	maprulebarT.append(mapruleoL);
	var maprulebarB = maprulebarT.clone().removeClass('maprulebar-top').addClass('maprulebar-bottom');
	$('#upperruinmap').prepend(maprulebarT);
	$('#upperruinmap').append(maprulebarB);
	$('#ruinName').html(data.map[py][px]['building']['name']);
	$('.ruinCoordsX').html(x);
	$('.ruinCoordsY').html(y);
	$('.ruinFloor').html('upper');

	if ($('#ruinCX').html() !== '' && $('#ruinCY').html() !== '' && $('#ruinCX').html() !== undefined && $('#ruinCY').html() !== undefined) {
		selectUpperRuinZone($('#lowerruinmap #x' + $('#ruinCX').html() + 'y' + $('#ruinCY').html()));
		data.crx = parseInt($('#ruinCX').html());
		data.cry = parseInt($('#ruinCY').html());
	} else {
		selectUpperRuinZone($('#upperruinmap #x0y1'));
		data.crx = 0;
		data.cry = 1;
	}

	// Lower Floor
	$('#lowerruinmap').html('');
	for (i = 1; i < 14; i++) { // lower floor don't have first row, so starts at 1
		var maprow = $(document.createElement('ul')).addClass('maprow');
		for (j = 0; j < 13; j++) {
			var ax = j;
			var ay = i;
			var mx = 'x' + j;
			var my = 'y' + i;
			var rx = j - 8;
			var ry = i;
			var rzd = null;

			var px = 'x' + x;
			var py = 'y' + y;
			maprow.append(generateRuinMapZone(i, j, data.map[py][px]['building']['ruin'], 'lower'));
		}
		var maprulerF = $(document.createElement('li')).addClass('mapruler').addClass('ruler_y' + ry).addClass('first').html(ry);
		var maprulerL = maprulerF.clone().removeClass('first').addClass('last');
		maprow.prepend(maprulerF);
		maprow.append(maprulerL);
		$('#lowerruinmap').append(maprow);
	}
	var maprulebarT = $(document.createElement('ul')).addClass('maprow').addClass('maprulebar').addClass('maprulebar-top');
	for (j = 0; j < 13; j++) {
		var maprulex = $(document.createElement('li')).addClass('mapruler').addClass('ruler_x' + (j - 7)).html(j - 7);
		maprulebarT.append(maprulex);
	}
	var mapruleoF = $(document.createElement('li')).addClass('mapcorner').addClass('first');
	var mapruleoL = mapruleoF.clone().removeClass('first').addClass('last');
	maprulebarT.prepend(mapruleoF);
	maprulebarT.append(mapruleoL);
	var maprulebarB = maprulebarT.clone().removeClass('maprulebar-top').addClass('maprulebar-bottom');
	$('#lowerruinmap').prepend(maprulebarT);
	$('#lowerruinmap').append(maprulebarB);
	$('#ruinName').html(data.map[py][px]['building']['name']);
	$('.ruinCoordsX').html(x);
	$('.ruinCoordsY').html(y);
	$('.ruinFloor').html('lower');

	if ($('#ruinCX').html() !== '' && $('#ruinCY').html() !== '' && $('#ruinCX').html() !== undefined && $('#ruinCY').html() !== undefined) {
		selectLowerRuinZone($('#lowerruinmap #x' + $('#ruinCX').html() + 'y' + $('#ruinCY').html()));
		data.crx = parseInt($('#ruinCX').html());
		data.cry = parseInt($('#ruinCY').html());
	} else {
		selectLowerRuinZone($('#lowerruinmap #x0y1'));
		data.crx = 0;
		data.cry = 1;
	}
}

function generateRuinMapZone(i, j, ruin, floor) {
	var ax = j;
	var ay = i;
	var mx = 'x' + j;
	var my = 'y' + i;
	var rx = j - 7;
	var ry = i;
	var rzd = null;
	var mapzone = $(document.createElement('li'));

	mapzone.addClass('mapzone').attr('rx', rx).attr('ry', ry).attr('ax', ax).attr('ay', ay);
	mapzone.attr('id', 'x' + rx + 'y' + ry);
	if (7 === j && 0 === i && floor === 'upper') {
		mapzone.addClass('ruinEntry');
	} else if (0 === i) {
		mapzone.addClass('ruinFirstRow');
	}

	if (ruin[floor] !== undefined && ruin[floor][my] !== undefined && ruin[floor][my][mx] !== undefined) {

		if (ruin[floor][my][mx]['items'] !== undefined && ruin[floor][my][mx]['items'].length > 0 && !(rx === 0 && ry === 0)) {
			var zoneItems = ruin[floor][my][mx]['items'];
			for (zi in zoneItems) {
				var zitem = zoneItems[zi];
				if (data.items[zitem.id] !== undefined) {
					var ritem = data.items[zitem.id];
					if (data.mapitems[ritem.category][ritem.id] !== undefined) {
						data.mapitems[ritem.category][ritem.id] += zitem.count;
					} else {
						data.mapitems[ritem.category][ritem.id] = zitem.count;
					}
				}
			}
		}

		var zone = ruin[floor][my][mx];

		if (ruin[floor][my][mx]['tile'] !== undefined) {
			mapzone.addClass('tile-' + ruin[floor][my][mx]['tile']);
		}
		if (ruin[floor][my][mx]['doorlock'] !== undefined && ruin[floor][my][mx]['doorlock'] !== "undefined") {
			mapzone.addClass('doorlock-' + ruin[floor][my][mx]['doorlock']);
		} else {
			if (ruin[floor][my][mx]['door'] !== undefined) {
				mapzone.addClass('door-' + ruin[floor][my][mx]['door']);
			}
			if (ruin[floor][my][mx]['lock'] !== undefined) {
				mapzone.addClass('lock-' + ruin[floor][my][mx]['lock']);
			}
		}
		if (ruin[floor][my][mx]['z'] !== undefined) {
			mapzone.attr('z', ruin[floor][my][mx]['z']);
			mapzone.addClass('zombie-' + ruin[floor][my][mx]['z']);
		} else {
			var zc = 0;
			mapzone.attr('z', 0);
			mapzone.addClass('danger0');
		}
		if (ruin[floor][my][mx]['comment'] !== undefined) {
			mapzone.attr('comment', ruin[floor][my][mx]['comment']);
			if (ruin[floor][my][mx]['comment'] !== '') {
				mapzone.addClass('with-comment');
			}
		} else {
			mapzone.attr('comment', '');
		}

		if (ruin[floor][my][mx]['updatedOn'] !== undefined && !(rx === 0 && ry === 0)) {
			var udate = new Date(ruin[floor][my][mx]['updatedOn'] * 1000);
			var cdate = new Date();
			var ydate = new Date();
			ydate.setDate(ydate.getDate() - 1);
			var bdate = new Date();
			bdate.setDate(bdate.getDate() - 2);
			if (udate.getDate() === cdate.getDate() && udate.getMonth() === cdate.getMonth()) {
				var utd = $(document.createElement('div')).addClass('zone-updated').addClass('zone-updated-today').attr('title', __('Heute aktualisiert'));
				mapzone.append(utd);
			} else if (udate.getDate() === ydate.getDate() && udate.getMonth() === ydate.getMonth()) {
				var utd = $(document.createElement('div')).addClass('zone-updated').addClass('zone-updated-yesterday').attr('title', __('Gestern aktualisiert'));
				mapzone.append(utd);
			} else if (udate.getDate() === bdate.getDate() && udate.getMonth() === bdate.getMonth()) {
				var utd = $(document.createElement('div')).addClass('zone-updated').addClass('zone-updated-b4yesterday').attr('title', __('Vorgestern aktualisiert'));
				mapzone.append(utd);
			}
		}
	} else if (i > 0) {
		/*mapzone.addClass('nyv');*/
	}

	return mapzone;
}

function saveRoute() {
	if ($('input[name="routeSave-name"]').val().trim().length === 0) {
		$('input[name="routeSave-name"]').addClass('error');
		ajaxInfo(__('Bitte einen Namen für die Route eingeben.'));
		return false;
	}
	if ($('input[name="routeSave-no"]:checked').val() === undefined || $('input[name="routeSave-no"]:checked').val() === 0 || $('input[name="routeSave-no"]:checked').val() > 5) {
		ajaxInfo(__('Bitte eine Route wählen.'));
		return false;
	}
	eval("var coX = clickRX" + $('input[name="routeSave-no"]:checked').val() + ";");
	eval("var coY = clickRY" + $('input[name="routeSave-no"]:checked').val() + ";");
	var routeString = '';
	for (i in coX) {
		routeString = routeString + coX[i] + '-' + coY[i] + '_';
	}
	if (routeString === '') {
		ajaxInfo(__('Diese Route wurde nicht eingezeichnet.'));
		return false;
	}
	var token = secureKey;
	$.ajax({
		type: "POST",
		url: "/map/updateRoutes",
		data: "key=" + token + "&action=ADDROUTE&rname=" + $('input[name="routeSave-name"]').val().trim() + "&route=" + routeString,
		success: function (msg) {
			$('#dynascript').append(msg);
			/*protectRuinBox(false);*/
		}
	});
}

function populateInterface(dictionary) {
	for (let k in dictionary) {
		let v = dictionary[k];
		let i = $('*[data-trans="'+k+'"]');
		if (i.length > 0) {
			for (let j = 0; j < i.length; j++) {
				let o = i[j];
				o.innerHTML = v;
			}
		}
		let m = $('*[data-transtitle="'+k+'"]');
		if (m.length > 0) {
			for (let l = 0; l < m.length; l++) {
				let p = m[l];
				p.title = v;
			}
		}
	}
}

function activateEventListeners() {
	// Event listeners.
	$('#map .mapzone').live({
														click: function () {
															data.cx = $(this).attr('rx');
															data.cy = $(this).attr('ry');
															selectZone($(this));
														},
														mouseover: function (e) {
															updateMapRulers($(this));
															if (mapHoverActive) {
																showMapHover(e, $(this).attr('id'));
															}
														},
														mouseout: function () {
															hideMapHover();
														},
														mousemove: function (e) {
															moveMapHover(e);
														}
													});
	$('#lowerruinmap .mapzone').live({
																click: function () {
																	selectLowerRuinZone($(this));
																	updateFloor("lower");
																},
																mouseover: function (e) {
																	updateLowerRuinMapRulers($(this));
																}
															});
	$('#upperruinmap .mapzone').live({
		click: function () {
			selectUpperRuinZone($(this));
			updateFloor("upper");
		},
		mouseover: function (e) {
			updateUpperRuinMapRulers($(this));
		}
	});

	// fetch item clicks
	$('#item-info .zone-item, #bank-info .zone-item').live({
																													 click: function (e) {
																														 e.stopPropagation();
																														 $(this).attr('state', (parseInt($(this).attr('state')) + Math.pow(-1, $(this).attr('state'))));
																														 highlightZonesWithItem();
																													 }
																												 });

	// fetch category clicks
	$('#item-info .zone-item-cat, #bank-info .zone-item-cat').live({
																																	 click: function () {
																																		 var newState = parseInt($(this).attr('state')) + Math.pow(-1, $(this).attr('state'));
																																		 $(this).attr('state', newState);
																																		 $('#' + $(this).attr('id') + ' .zone-item').attr('state', newState);
																																		 highlightZonesWithItem();
																																	 }
																																 });

	// item selector
	$('#item-selector .select-item').live({
																					click: function () {
																						data['saveItems'] = new zoneItemList(data.cx, data.cy);
																						var selItemId = $(this).attr('ref');
																						var zoneItemContainer = $('#zi_x' + data.cx + '_y' + data.cy);
																						if (zoneItemContainer !== undefined) {
																							zoneItemContainer.children('.zone-item').each(function () {
																								data.saveItems[$(this).attr('ref')] = $(this).attr('count');
																							});
																						} else {
																							return false;
																						}
																						if (data.saveItems[selItemId] !== undefined) {
																							data.saveItems[selItemId]++;
																							zoneItemContainer.children('.zone-item[ref="' + selItemId + '"]').attr('count', (data.saveItems[selItemId]));
																							zoneItemContainer.children('.zone-item[ref="' + selItemId + '"]').children('.count').html(data.saveItems[selItemId]);
																						} else {
																							data.saveItems[selItemId] = 1;
																							zoneItemContainer.append(createItemDisplay(selItemId, 1, 0));
																						}
																					}
																				});
	$('.zone-items .zone-item.changeable').live({
																								click: function () {
																									data['saveItems'] = new zoneItemList(data.cx, data.cy);
																									var selItemId = $(this).attr('ref');
																									var zoneItemContainer = $('#zi_x' + data.cx + '_y' + data.cy);
																									if (zoneItemContainer !== undefined) {
																										zoneItemContainer.children('.zone-item').each(function () {
																											data.saveItems[$(this).attr('ref')] = $(this).attr('count');
																										});
																									} else {
																										return false;
																									}
																									if (parseInt(data.saveItems[selItemId]) > 0) {
																										data.saveItems[selItemId]--;
																									}
																									zoneItemContainer.children('.zone-item[ref="' + selItemId + '"]').attr('count', (data.saveItems[selItemId]));
																									zoneItemContainer.children('.zone-item[ref="' + selItemId + '"]').children('.count').html(data.saveItems[selItemId]);
																								}
																							});
	// manual item update
	$('a.toggle-item-update').live({
																	 click: function (e) {
																		 e.preventDefault();
																		 $('#item-selector').toggleClass('hideme');
																		 $('.zone-items-footer').slideToggle(500);
																		 $('.zone-items .zone-item').toggleClass('changeable');
																	 }
																 });
	$('a.close-item-selector').live({
																		click: function (e) {
																			e.preventDefault();
																			$('#item-selector').addClass('hideme');
																			$('.zone-items-footer').slideUp(500);
																			$('.zone-items .zone-item').removeClass('changeable');
																		}
																	});

	$('a#toggle-routeSave').live({
																 click: function (e) {
																	 e.preventDefault();
																	 $('a#toggle-routeSave').hide();
																	 $('#routeSave-form').slideToggle(500);
																 }
															 });
	$('a#routeSave-cancel').live({
																 click: function (e) {
																	 e.preventDefault();
																	 $('#routeSave-form').slideUp(500, function () {
																		 $('a#toggle-routeSave').show();
																	 });
																 }
															 });
	$('a#routeSave-save').live({
															 click: function (e) {
																 e.preventDefault();
																 $('input[name="routeSave-name"]').removeClass('error');
																 saveRoute();
															 }
														 });

	$('a.toggle-building-update').live({
																			 click: function (e) {
																				 e.preventDefault();
																				 $('#building-selector').toggleClass('hideme');
																			 }
																		 });

	$('a.constructions-xml').live({
																	click: function (e) {
																		e.preventDefault();
																		$('.construction-table-man').addClass('hideme');
																		$('.construction-table-xml').removeClass('hideme');
																	}
																});
	$('a.constructions-man').live({
																	click: function (e) {
																		e.preventDefault();
																		$('.construction-table-xml').addClass('hideme');
																		$('.construction-table-man').removeClass('hideme');
																	}
																});

	$('a.ajaxsave').live({
												 click: function (e) {
													 e.preventDefault();
													 $('#item-selector').addClass('hideme');
													 $('.zone-items-footer').slideUp(500);
													 $('.zone-items .zone-item').removeClass('changeable');
													 if (data.saveItems !== undefined) {
														 //ajaxInfo('Einen Moment noch bitte');
														 var pas = '';
														 var pac = 0;
														 for (i in data.saveItems) {
															 if (i !== "x" && i !== "y") {
																 pas += 'i:' + i + ';i:' + data.saveItems[i] + ';';
																 pac++;
															 }
														 }
														 pas = 'a:' + pac + ':{' + pas + '}';
														 saveZoneItems(data.saveItems['x'], data.saveItems['y'], pas);
													 } else {
														 ajaxInfo(__('Keine Änderungen registriert.'));
													 }
												 }
											 });

	$('div.select-building').live({
																	click: function (e) {
																		e.preventDefault();
																		var bID = $(this).attr('ref');
																		saveBuildingGuess(data.cx, data.cy, bID);
																	}
																});

	$('div.ruinTile').live({
													 click: function () {
														 ajaxRuinUpdate('RUIN-TILE', $(this).attr('tile'));
													 }
												 });
	$('div.ruinDoorLock').live({
															 click: function () {
																 ajaxRuinUpdate('RUIN-DOORLOCK', $(this).attr('doorlock'));
															 }
														 });
	$('div.ruinDoor').live({
													 click: function () {
														 ajaxRuinUpdate('RUIN-DOOR', $(this).attr('door'));
													 }
												 });
	$('div.ruinLock').live({
													 click: function () {
														 ajaxRuinUpdate('RUIN-LOCK', $(this).attr('lock'));
													 }
												 });
	$('div.ruinZombie').live({
														 click: function () {
															 ajaxRuinUpdate('RUIN-ZOMBIE', $(this).attr('zombie'));
														 }
													 });
	$('#ruinComment-save').live({
																click: function () {
																	ajaxRuinUpdate('RUIN-COMMENT', $('#ruinComment').val());
																}
															});

	$('#update-myzone-button').live({
		click: function () {
			ajaxMyzoneUpdate();
		}
	});

	// generate bank data
	for (var a in data.bankSorted) {
		var b = data.bankSorted[a];
		var item = data.bank[b];
		if (data.items[b] !== undefined) {
			var raw_item = data.items[b];
			var brokenItem = item.broken === 1 ? ' broken' : '';
			var defItem = raw_item.category === 'Armor' ? ' defense' : '';
			$('#bank-info-' + raw_item.category).append('<div class="zone-item click' + brokenItem + defItem + '" state="0" ref="' + raw_item.id + '"><img src="' + data.system.icon + raw_item.image + '" title="' + raw_item.name[data.langcode] + ' (' + __('ID') + ': ' + Math.abs(raw_item.id) + ')" />&nbsp;' + item.count + '</div>');
		}
	}

	// tab switcher box
	$("ul#box-tabs li.box-tab").click(function (e) {
		e.preventDefault();
		$('li.box-tab').removeClass('active');
		$(this).addClass('active');
		var box = $(this).attr('ref');
		$('div.box-tab-content').addClass('hideme');
		$('div#' + box).removeClass('hideme');
		if (box !== 'zone-info') {
			$('#item-selector').addClass('hideme');
		}
	});
	$("ul#box-tabs li.box-spread").click(function (e) {
		e.preventDefault();
		$('#map-wrapper').css('opacity', .1);
		$('#item-selector').addClass('hideme');
		$('li.box-tab').addClass('hideme');
		$('div.box-tab-content').each(function () {
			$(this).removeClass('hideme').addClass('spread-out');
		});
		$(this).addClass('hideme');
		$("ul#box-tabs li.box-unspread").removeClass('hideme');
	});
	$("ul#box-tabs li.box-unspread").click(function (e) {
		e.preventDefault();
		$('div.box-tab-content').each(function () {
			$(this).removeClass('spread-out').addClass('hideme');
		});
		$('li.box-tab').removeClass('hideme');
		$('#map-wrapper').css('opacity', 1);
		$(this).addClass('hideme');
		$("ul#box-tabs li.box-spread").removeClass('hideme');
		$('#x' + (data.cx) + 'y' + (data.cy)).click();
	});
	// tab switcher tools
	$("ul#tools-tabs li").click(function (e) {
		e.preventDefault();
		$('li.tools-tab').removeClass('active');
		$(this).addClass('active')
		var box = $(this).attr('ref');
		$('div.tools-tab-content').addClass('hideme');
		$('div#' + box).removeClass('hideme');
	});

	// manual regeneration update
	$('a.ajaxlink').live({
												 click: function (e) {
													 e.preventDefault();
													 ajaxUpdate($(this).attr('id'), $('#zombie-count-input').val(), $('#maxzombie-count-input').val());
												 }
											 });
	// manual zombie update
	$('a.toggle-zombie-update').live({
																		 click: function (e) {
																			 e.preventDefault();
																			 $('.zombie-count-change').fadeIn(250, function () {
																			 });
																			 $('.toggle-zombie-update').fadeOut(250, function () {
																				 $('#UPDATE-ZOMBIES, #UPDATE-SCOUTZOMBIES').fadeIn(500);
																			 });
																		 }
																	 });
	$('a.toggle-maxzombie-update').live({
																				click: function (e) {
																					e.preventDefault();
																					$('.maxzombie-count-change').fadeIn(250, function () {
																					});
																					$('.toggle-maxzombie-update').fadeOut(250, function () {
																						$('.maxzombie-notice').fadeIn(500);
																						$('#UPDATE-MAXZOMBIES').fadeIn(1000);
																					});
																				}
																			});
	$('a#UPDATE-ZOMBIES').live({
															 click: function (e) {
																 e.preventDefault();
																 $('.zombie-count-change').fadeOut(250, function () {
																 });
																 $('#UPDATE-ZOMBIES').fadeOut(250, function () {
																	 $('.toggle-zombie-update').fadeIn(500);
																 });
																 //ajaxUpdate($(this).attr('id'),$('#zombie-count-input').val());
															 }
														 });
	$('a#UPDATE-MAXZOMBIES').live({
																	click: function (e) {
																		e.preventDefault();
																		$('.maxzombie-notice').fadeOut(125);
																		$('.maxzombie-count-change').fadeOut(250, function () {
																		});
																		$('#UPDATE-MAXZOMBIES').fadeOut(250, function () {
																			$('.toggle-maxzombie-update').fadeIn(500);
																		});
																	}
																});
	$('a#UPDATE-SCOUTZOMBIES').live({
																		click: function (e) {
																			e.preventDefault();
																			$('.zombie-count-change').fadeOut(250, function () {
																			});
																			$('#UPDATE-SCOUTZOMBIES').fadeOut(250, function () {
																				$('.toggle-zombie-update').fadeIn(500);
																			});
																		}
																	});
	$('a#ENTER-BUILDING').live({
															 click: function (e) {
																 e.preventDefault();
																 generateRuinMap($(this).attr('ocx'), $(this).attr('ocy'));
																 $('#ruinmap-wrapper').slideDown(500);
															 }
														 });
	$('div#ruinmap-wrapper-close').click(function () {
		$('#ruinmap-wrapper').slideUp(500);
	});
	$('.zombie-count-change.minus').live({
																				 click: function (e) {
																					 var cval = $('#zombie-count-input').val();
																					 var nval = parseInt(cval) + 1;
																					 $('#zombie-count-input').val(nval);
																					 $('#zombie-count-display').html(nval);
																				 }
																			 });
	$('.zombie-count-change.plus').live({
																				click: function (e) {
																					var cval = $('#zombie-count-input').val();
																					var nval = parseInt(cval) - 1;
																					if (nval < 0) {
																						nval = 0;
																					}
																					$('#zombie-count-input').val(nval);
																					$('#zombie-count-display').html(nval);
																				}
																			});
	$('.maxzombie-count-change.minus').live({
																						click: function (e) {
																							var cval = $('#maxzombie-count-input').val();
																							var nval = parseInt(cval) + 1;
																							$('#maxzombie-count-input').val(nval);
																							$('#maxzombie-count-display').html(nval);
																						}
																					});
	$('.maxzombie-count-change.plus').live({
																					 click: function (e) {
																						 var cval = $('#maxzombie-count-input').val();
																						 var nval = parseInt(cval) - 1;
																						 if (nval < 0) {
																							 nval = 0;
																						 }
																						 $('#maxzombie-count-input').val(nval);
																						 $('#maxzombie-count-display').html(nval);
																					 }
																				 });

	$('a.ajaxupdate').live({
													 click: function (e) {
														 e.preventDefault();
														 ajaxUpdate($(this).attr('id'), $('#storm-today').val(), 0);
													 }
												 });

	// fetch arrow keys
	document.addEventListener("keydown", e => {
		if (e.isComposing || e.keyCode === 229) {
			return;
		}
		if ($('#ruinmap-wrapper:visible').length === 1) {
			walkInTheRuin(e);
		} else {
			walkInTheDesert(e);
		}
	});

	// load color wheel
	$('#colorpicker').farbtastic('#opt-radius-color');

	// add radius
	$('#opt-radius-submit').click(function () {
		var color = $('input[name="opt-radius-color"]').val();
		var radius = $('input[name="opt-radius-number"]').val();
		var metric = $('input[name="opt-radius-metric"]:checked').val();
		addRadius(radius, metric, color);
	});

	// toggle display options
	$('.options-display-option').click(function () {
		var itemClass = $(this).attr('ref');
		$(this).toggleClass('active-option');
		$('.' + itemClass).toggleClass('hideme');
	});

	// toggle hover switch
	$('#options-display-zonehover.options-display-switch').click(function () {
		mapHoverActive = (mapHoverActive === true ? false : true);
		$(this).toggleClass('active-option');
	});
	// toggle geodir switch
	$('#options-display-geodir.options-display-switch').click(function () {
		toggleGeoDirDisplay();
		$(this).toggleClass('active-option');
	});

	// help switch
	$('.help-switch').click(function () {
		$('.help-switch').toggleClass('active');
		$('#help-section').toggleClass('hidefaq showfaq');
	});
	$('#help-section ul li').click(function () {
		$('#help-section ul li').removeClass('active');
		$(this).addClass('active');
	});
	// mode switch
	$('.mode-switch').click(function () {
		$('.mode-switch').removeClass('active-mode');
		$(this).addClass('active-mode');
		if ($(this).attr('id') === 'mode-normal') {
			$('#canvasPlanDiv').addClass('hideme');
			$('#canvasDrawDiv').addClass('hideme');
			$('#tools').addClass('hideme');
			$('#cape').addClass('hideme');
			$('#box').removeClass('hideme');
		} else if ($(this).attr('id') === 'mode-planner') {
			$('#tab-tools-planner').click();
		} else if ($(this).attr('id') === 'mode-draw') {
			$('#tab-tools-colors').click();
		} else if ($(this).attr('id') === 'mode-cape') {
			$('#canvasPlanDiv').addClass('hideme');
			$('#canvasDrawDiv').addClass('hideme');
			$('#box').addClass('hideme');
			$('#tools').addClass('hideme');
			$('#cape').removeClass('hideme');
		}
	});
	$('#tab-tools-colors').click(function () {
		$('.mode-switch').removeClass('active-mode');
		$('#mode-draw').addClass('active-mode');
		$('#canvasPlanDiv').addClass('hideme');
		$('#canvasDrawDiv').removeClass('hideme');
		$('#box').addClass('hideme');
		$('#tools').removeClass('hideme');
	});
	$('#tab-tools-planner').click(function () {
		$('.mode-switch').removeClass('active-mode');
		$('#mode-planner').addClass('active-mode');
		$('#canvasDrawDiv').addClass('hideme');
		$('#canvasPlanDiv').removeClass('hideme');
		$('#box').addClass('hideme');
		$('#tools').removeClass('hideme');
	});

	// toggle display expeditions
	$('.exp-item').live({
												click: function () {
													$('.exp-item').removeClass('active-option');
													var rid = $(this).attr('id');
													$(this).toggleClass('active-option');
													highlightRoute(rid);
												}
											});

	$('#box').slideDown(500);

	$('li.city').live({
											mouseover: function () {
												toggleGeoDirDisplay();
											},
											mouseout: function () {
												toggleGeoDirDisplay();
											}
										});

	$('.ruinOption').live({
													mouseover: function () {
														$('#ruinHoverInfo').addClass('hovering').html($(this).attr('title'));
													},
													mouseout: function () {
														$('#ruinHoverInfo').removeClass('hovering').html('');
													}
												});

	$('#options-save').click(function () {
		saveSettings();
	});
	$('#options-reset').click(function () {
		saveSettings(true);
	});

	$('#radius-save').click(function () {
		saveRadii();
	});
	$('#radius-reset').click(function () {
		saveRadii(true);
	});

}

function saveSettings(reset = false) {
	var settings = 0;
	var options = {
		'zonehover': 1,
		'geodir': 2,
		'driedzone': 4,
		'fullzone': 8,
		'citizens': 16,
		'zombies': 32,
		'uptodate': 64,
		'stormzone': 128,
	}
	for(let o in options) {
		let option = document.getElementById('options-display-' + o);
		if (option.classList.contains('active-option')) {
			settings += options[o];
		}
	}
	if (reset) {
		localStorage.setItem('fm_settings', 217);
		loadSettings();
	}
	else {
		localStorage.setItem('fm_settings', settings);
	}
	ajaxInfo(__('settings-saved'));
}
function loadSettings() {
	/*
	* SETTINGS OVERVIEW
	*
	* options-display-*
	*
	* zonehover 		= 1			x
	* geodir 				= 2
	* driedzone 		= 4
	* fullzone 			= 8			x
	* citizens 			= 16		x
	* zombies 			= 32
	* uptodate 			= 64		x
	* stormzone			= 128		x
	*
	* x = active by default (217)
	*
	* */
	var settings = localStorage.getItem('fm_settings');
	if (!settings) {
			localStorage.setItem('fm_settings', 217);
			settings = 217;
	}
	if (settings) {
		var code = parseInt(settings);
		if (code >= 128) {
			$('#options-display-stormzone').click();
			code -= 128;
		}
		if (code >= 64) {
			$('#options-display-uptodate').click();
			code -= 64;
		}
		if (code >= 32) {
			$('#options-display-zombies').click();
			code -= 32;
		}
		if (code >= 16) {
			$('#options-display-citizens').click();
			code -= 16;
		}
		if (code >= 8) {
			$('#options-display-fullzone').click();
			code -= 8;
		}
		if (code >= 4) {
			$('#options-display-driedzone').click();
			code -= 4;
		}
		if (code >= 2) {
			$('#options-display-geodir').click();
			code -= 2;
		}
		if (code >= 1) {
			$('#options-display-zonehover').click();
			code -= 1;
		}
		if (code !== 0) {
			console.log('Settings corrupt');
		}
	}
}
function saveRadii(reset = false) {
	let radii = [];
	if (reset) {
		localStorage.removeItem('fm_radii');
		loadRadii();
	}
	else {
		document.querySelectorAll('.radius-list-item').forEach(function (el) {
			//console.log(el.dataset.range, el.dataset.metric, el.dataset.color);
			radii.push([el.dataset.range, el.dataset.metric, el.dataset.color, el.dataset.name].join('|'));
		});
		localStorage.setItem('fm_radii', radii.join('+'));
	}
	ajaxInfo(__('radii-saved'));
}
function loadRadii() {
	document.querySelectorAll('.radius-list-item').forEach(function (el) {
		el.click();
	});
	var radii_serial = localStorage.getItem('fm_radii');
	if (!radii_serial) {
		let wt_ap = 9;
		if (data.hasOwnProperty('constructions') && data.constructions.hasOwnProperty('103')) {
			var wt = data.constructions[103];
			if (wt.level) {
				if (parseInt(wt.level) === 5) {
					wt_ap = 11;
				} else if (parseInt(wt.level) === 4) {
					wt_ap = 10;
				}
			}
		}
		let radii = [];
		radii.push([wt_ap,'ap','#00ff66',__('default-radius')].join('|'));
		radii.push([11,'km','#00ffff',__('hero-return')].join('|'));
		radii_serial = radii.join('+');
		localStorage.setItem('fm_radii', radii_serial);
	}
	if (radii_serial) {
		let radii = radii_serial.split('+');
		for (let r in radii) {
			let radius = radii[r];
			if (radius !== undefined && radius !== '') {
				let rdata = radius.split('|');
				addRadius(rdata[0],rdata[1],rdata[2],rdata[3]);
			}
		}
	}
}

function initiateFM() {
	var system = data['system'];
	var username = system['owner_name'];
	var langcode = data['langcode'];
	window.softreloads = 0;
	populateInterface(trans[langcode]);
	jsIn.addDict(dict[langcode]);
	$('#owner-name').html(username);
	$('#townName').html(system['gamename']);
	$('#townDay').html(__('Tag') + ' ' + system['day']);
	$('#townID').html(__('ID') + ': ' + system['gameid']);
	$('#townSpy a').attr('href', fm_url + 'spy/town/' + system['gameid']);
	for (i in system['days']) {
		var hislink = $(document.createElement('a')).attr('href', fm_url + 'spy/town/' + system['gameid'] + '/' + system['days'][i]).html(system['days'][i]);
		$('#townHistory').append(hislink);
	}
	$('#townBar').slideDown(500);

	if (data.spy !== undefined && data.spy === 1) {
		window.ajaxUpdate = function () {
			return false;
		}
		document.getElementById('update-myzone').remove();
	}

	if (data.system.error_code !== undefined) {
		$('#box').remove();
		$('.mode-switch').remove();
		$('#fm-content').html('<p style="background:rgba(0,0,0,.7);padding:6px;border-radius:6px;color:#c00;">' + __('Es ist ein Fehler aufgetreten. Von MyHordes wurde folgender Fehlercode gesendet') + ': <strong>' + data.system.error_code + '</strong>.</p>');
	}
	$('#canvasDrawDiv').css('width', (data.width * 32) + 'px');
	$('#canvasDrawDiv').css('height', (data.height * 32) + 'px');
	$('#canvasPlanDiv').css('width', (data.width * 32) + 'px');
	$('#canvasPlanDiv').css('height', (data.height * 32) + 'px');

	if (data.spy !== undefined && data.spy === 1) {
		$('#ruinInfoBox').remove();
	}

	// Map generation
	for (i = 0; i < data['height']; i++) {
		var maprow = $(document.createElement('ul')).addClass('maprow');
		for (j = 0; j < data['width']; j++) {
			var ax = j;
			var ay = i;
			var mx = 'x' + j;
			var my = 'y' + i;
			var rx = j - data['tx'];
			var ry = data['ty'] - i;
			var rzd = null;
			maprow.append(generateMapZone(i, j));
		}
		var maprulerF = $(document.createElement('li')).addClass('mapruler').addClass('ruler_y' + ry).addClass('first').html(ry);
		var maprulerL = maprulerF.clone().removeClass('first').addClass('last');
		maprow.prepend(maprulerF);
		maprow.append(maprulerL);
		$('#map').append(maprow);
	}
	var maprulebarT = $(document.createElement('ul')).addClass('maprow').addClass('maprulebar').addClass('maprulebar-top');
	for (j = 0; j < data['width']; j++) {
		var maprulex = $(document.createElement('li')).addClass('mapruler').addClass('ruler_x' + (j - data['tx'])).html(j - data['tx']);
		maprulebarT.append(maprulex);
	}
	var mapruleoF = $(document.createElement('li')).addClass('mapcorner').addClass('first');
	var mapruleoL = mapruleoF.clone().removeClass('first').addClass('last');
	maprulebarT.prepend(mapruleoF);
	maprulebarT.append(mapruleoL);
	var maprulebarB = maprulebarT.clone().removeClass('maprulebar-top').addClass('maprulebar-bottom');
	$('#map').prepend(maprulebarT);
	$('#map').append(maprulebarB);

	// generate item data
	for (c in data.mapitems) {
		var itemsubbox = $('#item-info-' + data.mapitems[c]['cat']);
		//alert(data.mapitems[c]['cat']);
		for (d in data.mapitems[c]) {

			if (d !== 'cat') {
				var itemcount = data.mapitems[c][d];
				if (data.items[Math.abs(d)] !== undefined && itemcount > 0) {
					var raw_item = data.items[Math.abs(d)];
					var brokenItem = d < 0 ? ' broken' : '';
					var defItem = raw_item.category === 'Armor' ? ' defense' : '';
					itemsubbox.append('<div class="zone-item click' + brokenItem + defItem + '" state="0" ref="' + raw_item.id + '"><img src="' + data.system.icon + raw_item.image + '" title="' + raw_item.name[data.langcode] + ' (ID: ' + Math.abs(raw_item.id) + ')" />&nbsp;' + itemcount + '</div>');
				}
			}
		}
	}

	activateEventListeners();

	// populate item selector
	populateItemSelector();
	// populate building selector
	populateBuildingSelector();
	// populate info tabs
	generateStormList();
	generateExpeditionList();
	generateRuinList();
	generateCitizenList();
	generateConstructionList();

	loadSettings();
	loadRadii();

	// "click".current-zone
	data.cx = data.ox - data.tx;
	data.cy = data.ty - data.oy;
	$('#x' + (data.cx) + 'y' + (data.cy)).click();
}

/*
 * ######################
 * # GENERATE THE MAP   #
 * # AND ALL INFO BOXES #
 * ######################
 */
$(document).ready(function () {
	initiateFM();
}); // END GENERATE

